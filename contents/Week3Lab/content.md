# [Spotted Lake, British Columbia, Canada](img/spotted_lake.jpg)

![Spotted Lake, British Columbia, Canada](img/spotted_lake.jpg)

Spotted Lake has long been revered by the native Okanagan (Syilx) people and it’s easy to see why they think of it as sacred. In the summer the water of the lake evaporates and small mineral pools are left behind, each one different in colour to the next. The unique lake can be viewed on Highway 3, northwest of the small town of Osoyoos, although visitors are asked not to trespass on tribal land.

## [The Giant’s Causeway, Northern Ireland](img/giants_causeway.jpg)

![The Giant’s Causeway, Northern Ireland](img/giants_causeway.jpg)

Sixty million years ago a huge volcanic eruption spewed out a mass of molten basalt, which then solidified and contracted as it cooled, creating the cracks that can be seen today. There are an estimated 37,000 polygon columns at this World Heritage Site, so geometrically perfect that local legend has it they were created by a giant.

### [Thor’s Well, Oregon, USA](img/thors_well.jpg)

![Thor’s Well, Oregon, USA](img/thors_well.jpg)

In rough conditions at Thor’s Well, also known as Spouting Horn, the surf rushes into the gaping sinkhole and then shoots upwards with great force. It can be viewed by taking the Captain Cook Trail from the Cape Perpetua Scenic Area visitor centre – but for your own safety stay well back, especially at high tide or during winter storms.

#### [Pamukkale, Turkey](img/pamukkale.jpg)

![Pamukkale, Turkey](img/pamukkale.jpg)

A remarkable UNESCO World Heritage Site in southwest Turkey, a visit to Pamukkale (Cotton Palace) also takes in the ancient ruins of Hierapolis, the once great city that was built around it. Water cascades from natural springs and down the white travertine terraces and forms stunning thermal pools perfect for a quick dip.

##### [Lake Hillier, Western Australia](img/lake_hillier.jpg)

![Lake Hillier, Western Australia](img/lake_hillier.jpg)

This remarkable lake was discovered in 1802 on the largest of the islands in Western Australia’s Recherche Archipelago. The lake keeps its deep pink colour year-round, which some scientists say is down to high salinity combined with the presence of a salt-loving algae species known as Dunaliella salina and pink bacteria known as halobacteria.

###### [Badab-e-Surt, Iran](img/badab_e-surt.jpg)

![Badab-e-Surt, Iran](img/badab_e-surt.jpg)

These beautiful travertine terraces in northern Iran are an incredible natural phenomenon that developed over thousands of years. Travertine is a type of limestone formed from the calcium deposit in flowing water, and in this case it’s two hot springs with different mineral properties. The unusual reddish colour of the terraces is down to the high content of iron oxide in one of the springs.
