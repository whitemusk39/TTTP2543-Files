<?php

if (isset($_POST['biodata_validate'])) {

    /**
    *   Initialise variables.
    *
    *   Use array because it will be much easier to
    *   manage and used together with foreach.
    */

    $vars = array(
        "name" => "",
        "age" => "",
        "sex" => "",
        "address" => "",
        "email" => "",
        "dob" => "",
        "height" => "",
        "phone" => "",
        "color" => "",
        "fbtwig" => "",
        "univ" => "",
        "matricnum" => "",
    );

    $count_error = 0;
    $msg = "";

    // Fill in the variables with their values
    foreach ($_POST as $key=>$value) {
        if ($key !== "biodata_validate" || $value !== "Save My Biodata") {
            if (isset($value) && !empty($value)) {
                $vars[$key] = $value;
            } else {
                $msg .= "Error: Field '". $fieldName ."' is not filled in.<br />";
                $count_error++;
            }
        }
    }

    // Display error messages if there's any error.
    if ($count_error > 0) {
        echo $msg;
        echo "$count_error error(s) detected.<br />";
        die();
    }

    // Begin SQL statements
    $sql1 = "INSERT INTO biodata(";
    $sql2 = ") VALUES(";

    // Get the final key.
    end($vars);
    $last = key($vars);

    foreach ($vars as $key=>$value) {

        // Check if value is not numeric
        if (!is_numeric($value)) {
            // Not numeric so quote ('')
            $value = "'".$value."'";
        }

        // Check if final element in loop
        if ($key === $last) {
            $sql1 .= $key;
            $sql2 .= $value.")";
        } else {
            $sql1 .= $key.", ";
            $sql2 .= $value.", ";
        }
    }

    // Combine both strings to get a complete SQL statement
    echo $sql1.$sql2;
} else {
    echo "ERROR: You executed the wrong PHP script. Please contact the web administrator.";
    die();
}
