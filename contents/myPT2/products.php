<?php
include_once 'products_crud.php';
?>

    <!DOCTYPE html>
    <html>

    <head>
        <title>Antiques Delight : Products</title>
    </head>

    <body>
        <center>
            <a href="index.php">Home</a> |
            <a href="products.php">Products</a> |
            <a href="customers.php">Customers</a> |
            <a href="staffs.php">Staffs</a> |
            <a href="orders.php">Orders</a>
            <hr>
            <form action="products.php" method="post">
                Product ID
                <input name="pid" type="text" value="<?php if (isset($_GET['edit'])) {
                    echo $editrow['fld_product_num'];
} ?>">
                <br> Name
                <input name="name" type="text" value="<?php if (isset($_GET['edit'])) {
                    echo $editrow['fld_product_name'];
} ?>">
                <br> Price
                <input name="price" type="text" value="<?php if (isset($_GET['edit'])) {
                    echo $editrow['fld_product_price'];
} ?>">
                <br> Material
                <select name="material">
				<?php
				// Read
                try {
                    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $conn->prepare("SELECT * FROM tbl_materials_a155652_pt2");
                    $stmt->execute();
                    $result = $stmt->fetchAll();
                } catch (PDOException $e) {
                    echo "Error: " . $e->getMessage();
                }
				foreach ($result as $readrow) {
					echo "<option value='".$readrow['fld_material_num']."'";
					if (isset($_GET['edit'])) {
						if ($editrow['fld_material']==$readrow['fld_material_num']) {
                            echo "selected";
                        }
					}
					echo ">";
					echo $readrow['fld_material_name'];
					echo "</option>";
				}
				
				?>
                </select>
				<br> Period
                <select name="period">
				<?php
				// Read
                try {
                    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $conn->prepare("SELECT * FROM tbl_periods_a155652_pt2");
                    $stmt->execute();
                    $result = $stmt->fetchAll();
                } catch (PDOException $e) {
                    echo "Error: " . $e->getMessage();
                }
				foreach ($result as $readrow) {
					echo "<option value='".$readrow['fld_period_num']."'";
					if (isset($_GET['edit'])) {
						if ($editrow['fld_period']==$readrow['fld_period_num']) {
                            echo "selected";
                        }
					}
					echo ">";
					echo $readrow['fld_period_name'];
					echo "</option>";
				}
				
				?>
                </select>
                <br> Quantity
                <input name="quantity" type="text" value="<?php if (isset($_GET['edit'])) {
                    echo $editrow['fld_quantity'];
} ?>">
                <br>
                <?php if (isset($_GET['edit'])) {
    ?>
                <input type="hidden" name="oldpid" value="<?php echo $editrow['fld_product_num']; ?>">
                <button type="submit" name="update">Update</button>
                <?php
} else {
        ?>
                    <button type="submit" name="create">Create</button>
                    <?php
} ?>
                        <button type="reset">Clear</button>
            </form>
            <hr>
            <table border="1">
                <tr>
                    <td>Product ID</td>
                    <td>Name</td>
                    <td>Price</td>
                    <td>Material</td>
                    <td>Manufacturing Period</td>
                    <td></td>
                </tr>
                <?php
				// Read
                try {
                    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $stmt = $conn->prepare("SELECT * FROM tbl_products_a155652_pt2, tbl_periods_a155652_pt2, tbl_materials_a155652_pt2 WHERE tbl_products_a155652_pt2.fld_material = tbl_materials_a155652_pt2.fld_material_num AND tbl_products_a155652_pt2.fld_period = tbl_periods_a155652_pt2.fld_period_num");
                    $stmt->execute();
                    $result = $stmt->fetchAll();
                } catch (PDOException $e) {
                    echo "Error: " . $e->getMessage();
                }
                foreach ($result as $readrow) {
                    ?>
                    <tr>
                        <td>
                            <?php echo $readrow['fld_product_num']; ?>
                        </td>
                        <td>
                            <?php echo $readrow['fld_product_name']; ?>
                        </td>
                        <td>
                            <?php echo $readrow['fld_product_price']; ?>
                        </td>
                        <td>
                            <?php echo $readrow['fld_material_name']; ?>
                        </td>
                        <td>
                            <?php echo $readrow['fld_period_name']; ?>
                        </td>
                        <td>
                            <a href="products_details.php?pid=<?php echo $readrow['fld_product_num']; ?>">Details</a>
                            <a href="products.php?edit=<?php echo $readrow['fld_product_num']; ?>">Edit</a>
                            <a href="products.php?delete=<?php echo $readrow['fld_product_num']; ?>" onclick="return confirm('Are you sure to delete?');">Delete</a>
                        </td>
                    </tr>
                    <?php
                }
                $conn = null;
    ?>

            </table>
        </center>
    </body>

    </html>
