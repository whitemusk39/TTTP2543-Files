<?php
	include_once('session.php');
	verifySession();
?>
<!DOCTYPE html>
    <html>

    <head>
        <?php include_once('head.php'); ?>
    </head>

    <body>
        <?php include_once('nav_bar.php'); ?>

        <div class="container">

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                          <?php
                          include_once 'orders_details_crud.php';

													try {
												      $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
												      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
												      $stmt = $conn->prepare("SELECT * FROM tbl_orders_a155652_pt2, tbl_staffs_a155652_pt2,
														tbl_customers_a155652_pt2 WHERE
														tbl_orders_a155652_pt2.fld_staff_num = tbl_staffs_a155652_pt2.fld_staff_num AND
														tbl_orders_a155652_pt2.fld_customer_num = tbl_customers_a155652_pt2.fld_customer_num AND
														fld_order_num = :oid");
												      $stmt->bindParam(':oid', $oid, PDO::PARAM_STR);
												      $oid = $_GET['oid'];
												      $stmt->execute();
												      $readrow = $stmt->fetch(PDO::FETCH_ASSOC);
												  } catch (PDOException $e) {
												      echo "Error: " . $e->getMessage();
												  }
												  $conn = null;
                          ?>
                            <h1 class="card-title display-4">Order Details</h1>
                            <h6 class="card-subtitle mb-2 text-muted">View your order details.</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <form>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label>Order ID</label>
                                            <input class="form-control" name="oid" type="text" value="<?php echo $readrow['fld_order_num']; ?>" placeholder="Order ID" style="text-transform: capitalize;" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Date</label>
                                            <input style="text-transform: capitalize;" class="form-control" name="orderdate" type="text" value="<?php echo $readrow['fld_order_date']; ?>" placeholder="Order Date" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Staff</label>
                                            <input class="form-control" name="staff" type="text" value="<?php echo $readrow['fld_staff_name']; ?>" placeholder="Staff Name" style="text-transform: capitalize;" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Customer</label>
                                            <input class="form-control" name="cust" type="text" value="<?php echo $readrow['fld_customer_name']; ?>" placeholder="Customer Name" style="text-transform: capitalize;" readonly>
                                        </div>
                                    </div>
																		<a href="invoice.php?oid=<?php echo $_GET['oid']; ?>" target="_blank" class="btn btn-primary">Generate Invoice</a>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title display-4">Order Basket</h1>
                            <h6 class="card-subtitle mb-2 text-muted">Products in this order.</h6>
                        </div>
												<ul class="list-group list-group-flush">
							            <li class="list-group-item">
							              <form action="orders_details.php" method="post">
							                <div class="form-row">
							                  <div class="form-group col-md-11">
							                    <label for="imaterial">Material</label>
																	<select name="pid" class="form-control" style="text-transform: capitalize;" id="imaterial">
																		<?php
												    try {
												        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
												        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
												        $stmt = $conn->prepare("SELECT * FROM tbl_products_a155652_pt2");
												        $stmt->execute();
												        $result = $stmt->fetchAll();
												    } catch (PDOException $e) {
												        echo "Error: " . $e->getMessage();
												    }
												    foreach ($result as $productrow) {
												        ?>
																			<option value="<?php echo $productrow['fld_product_num']; ?>">
																				<?php echo $productrow['fld_product_name']; ?>
																			</option>
																			<?php
												    }
												    $conn = null;
												    ?>
																	</select>
							                  </div>
							                  <div class="form-group col-md-1">
							                    <label for="iquantity">Quantity</label>
							                    <input class="form-control" id="iquantity" name="quantity" type="number" min="1" value="1" placeholder="1" step="1" required>
							                  </div>
							                </div>
							                <input type="hidden" name="oid" value="<?php echo $readrow['fld_order_num']; ?>">
							                <button type="submit" name="addproduct" value="addproduct"  class="btn btn-primary">Add Product</button>
							                <button type="reset" class="btn">Clear</button>
							              </form>
							            </li>
							          </ul>
                        <table class="table table-hover table-bordered table-responsive" style="text-transform: capitalize; background-color: white; margin-bottom: 0;">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" width="95%">Product</th>
                                    <th scope="col" width="4%">Quantity</th>
                                    <th scope="col" width="1%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    // Read
                                    try {
																			$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
																			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
																			$stmt = $conn->prepare("SELECT * FROM tbl_orders_details_a155652_pt2,
																		tbl_products_a155652_pt2 WHERE
																		tbl_orders_details_a155652_pt2.fld_product_num = tbl_products_a155652_pt2.fld_product_num AND
																	fld_order_num = :oid");
																			$stmt->bindParam(':oid', $oid, PDO::PARAM_STR);
																			$oid = $_GET['oid'];
																			$stmt->execute();
																			$result = $stmt->fetchAll();
                                    } catch (PDOException $e) {
                                        echo "Error: " . $e->getMessage();
                                    }
                                    foreach ($result as $readrow) {
                                        ?>
                                            <tr>
                                                <td>
                                                    <?php echo $readrow['fld_product_name']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $readrow['fld_order_detail_quantity']; ?>
                                                </td>
                                                <td style="text-align: center">
                                                    <a href="orders_details.php?delete=<?php echo $readrow['fld_order_detail_num']; ?>&oid=<?php echo $_GET['oid']; ?>"><span class="oi oi-delete" title="Delete" aria-hidden="true" onclick="return confirm('Are you sure to delete?');"></span></a>
                                                </td>
                                            </tr>
                                            <?php
                                    }
                                    $conn = null;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <?php include_once('footer.php'); ?>
        <?php include_once('bootstrap_js.php'); ?>
    </body>

    </html>
