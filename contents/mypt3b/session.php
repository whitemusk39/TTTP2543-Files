<?php
  include("database.php");
  session_start();

  function verifySession() {
    if(!isset($_SESSION['login_user'])) {
      header("location: login.php");
    }
  }

  if(isset($_SESSION['login_user'])) {
    $name = "";

    try {
      $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("SELECT fld_staff_name FROM tbl_staffs_a155652_pt2 WHERE fld_staff_username = :user");
      $stmt->bindParam(':user', $user, PDO::PARAM_STR);

      $user = $_SESSION['login_user'];

      $stmt->execute();
      $result = $stmt->fetch(PDO::FETCH_ASSOC);

      $name = $result['fld_staff_name'];
    } catch (PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
  }
