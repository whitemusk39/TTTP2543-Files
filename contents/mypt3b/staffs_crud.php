<?php

function titleCase($s)
{
    $s = ucwords(strtolower($s));
    $start = intval(strpos($s, stristr($s, "(")));

    if ($start <> 0) {
        $end = intval(strpos($s, stristr($s, ")")));
        $result = substr($s, ($start+1), (($end - $start)-1));
        $s = str_replace($result, strtoupper($result), $s);
    }

    return $s;
}

include_once 'database.php';

$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if (isset($_POST['update'])) {

    /**
    *   Initialise variables.
    *
    *   Use array because it will be much easier to
    *   manage and used together with foreach.
    */
    $vars = array(
        "sid" => "",
        "name" => "",
        "susername" => "",
        "currpass" => "",
        "email" => ""
    );

    $count_error = 0;
    $msg = "";

    /**
    *    Validate submitted data.
    *
    *   If empty show error message,
    *   else put in local variables.
    */

    foreach ($_POST as $key=>$value) {

        /**
        *   Set the fieldName.
        *
        *   To make the error messages prettier. It will
        *   use the field's title instead of the field's ID.
        */

        $fieldName = "";
        switch ($key) {
            case "sid":
                $fieldName = "Staff ID";
                break;
            case "name":
                $fieldName = "Staff Name";
                break;
            case "currpass":
                $fieldName = "Current Password";
                break;
            case "susername":
                $fieldName = "Username";
                break;
            case "email":
                $fieldName = "Email";
                break;
            default:
                $fieldName = $key;
                break;
        }

        // Check if the value is set and not empty
        if ($key == "sid"){
          try {
              $stmt = $conn->prepare("SELECT fld_staff_num FROM tbl_staffs_a155652_pt2 WHERE fld_staff_num = :findsid");
              $stmt->bindParam(':findsid', $findsid, PDO::PARAM_STR);
              $findsid = $value;
              $stmt->execute();
              $result = $stmt->fetch(PDO::FETCH_ASSOC);
              if(empty($result["fld_staff_num"]) && !isset($result["fld_staff_num"])) {
                $vars[$key] = $value;
              } if($_POST['oldsid'] == $result["fld_staff_num"]) {
                $vars[$key] = $value;
              } else {
                $msg .= "Error: Staff ID already exist!<br />";
                $count_error++;
              }
          } catch (PDOException $e) {
              $msg .= "Error: " . $e->getMessage() . "<br />";
          }
        } else if ($key == "currpass") {
          if(!isset($_POST['currpass']) && empty($_POST['currpass'])) {
            $msg .= "Error: Please enter your current password!<br />";
            $count_error++;
          } else {
            try {
                $stmt = $conn->prepare("SELECT fld_staff_password, fld_staff_num FROM tbl_staffs_a155652_pt2 WHERE fld_staff_num = :findid");
                $stmt->bindParam(':findid', $findid, PDO::PARAM_STR);
                $findid = $_POST['oldsid'];
                $stmt->execute();
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                if($result['fld_staff_num'] == $_POST['oldsid']) {
                  if(password_verify($_POST['currpass'], $result["fld_staff_password"])) {
                    $vars[$key] = $value;
                  } else {
                    $msg .= "Error: Current Password does not match!<br />";
                    $count_error++;
                  }
                }
            } catch (PDOException $e) {
                $msg .= "Error: " . $e->getMessage() . "<br />";
            }
          }
        } else if ($key == "susername") {
          try {
              $stmt = $conn->prepare("SELECT fld_staff_num, fld_staff_username FROM tbl_staffs_a155652_pt2 WHERE fld_staff_username = :user");
              $stmt->bindParam(':user', $user, PDO::PARAM_STR);
              $user = $value;
              $stmt->execute();
              $result = $stmt->fetch(PDO::FETCH_ASSOC);
              if(empty($result["fld_staff_num"]) && !isset($result["fld_staff_num"])) {
                $vars[$key] = $value;
              } else if ($result["fld_staff_num"] == $_POST['oldsid']) {
                $vars[$key] = $value;
              } else {
                $msg .= "Error: Username already exist!<br />";
                $count_error++;
              }
          } catch (PDOException $e) {
              $msg .= "Error: " . $e->getMessage() . "<br />";
          }
        } else if (isset($value) && !empty($value)) {
            $vars[$key] = $value;
        } else {
          if($key != "newpass") {
            $msg .= "Error: Field '". $fieldName ."' is not filled in.<br />";
            $count_error++;
          }
        }
    }

    // Display error messages if there's any error.
    if ($count_error > 0) {
        echo $msg;
        echo "$count_error error(s) detected.<br />";
        echo "<button class='btn' onclick='history.go(-1);'>Back </button>";
        die();
    }
  }

if (isset($_POST['create'])) {

    /**
    *   Initialise variables.
    *
    *   Use array because it will be much easier to
    *   manage and used together with foreach.
    */
    $vars = array(
        "sid" => "",
        "name" => "",
        "susername" => "",
        "spassword" => "",
        "email" => ""
    );

    $count_error = 0;
    $msg = "";

    /**
    *    Validate submitted data.
    *
    *   If empty show error message,
    *   else put in local variables.
    */

    foreach ($_POST as $key=>$value) {

        /**
        *   Set the fieldName.
        *
        *   To make the error messages prettier. It will
        *   use the field's title instead of the field's ID.
        */

        $fieldName = "";
        switch ($key) {
            case "sid":
                $fieldName = "Staff ID";
                break;
            case "name":
                $fieldName = "Staff Name";
                break;
            case "spassword":
                $fieldName = "Password";
                break;
            case "susername":
                $fieldName = "Username";
                break;
            case "email":
                $fieldName = "Email";
                break;
            default:
                $fieldName = $key;
                break;
        }

        // Check if the value is set and not empty
        if ($key == "sid"){
          try {
              $stmt = $conn->prepare("SELECT fld_staff_num FROM tbl_staffs_a155652_pt2 WHERE fld_staff_num = :findsid");
              $stmt->bindParam(':findsid', $findsid, PDO::PARAM_STR);
              $findsid = $value;
              $stmt->execute();
              $result = $stmt->fetch(PDO::FETCH_ASSOC);
              if(empty($result["fld_staff_num"]) && !isset($result["fld_staff_num"])) {
                $vars[$key] = $value;
              } else if(isset($_POST['update'])) {
                if($_POST['oldsid'] == $result["fld_staff_num"]) {
                  $vars[$key] = $value;
                }
              } else {
                $msg .= "Error: Staff ID already exist!<br />";
                $count_error++;
              }
          } catch (PDOException $e) {
              $msg .= "Error: " . $e->getMessage() . "<br />";
          }
        } else if ($key == "susername") {
          try {
              $stmt = $conn->prepare("SELECT fld_staff_num FROM tbl_staffs_a155652_pt2 WHERE fld_staff_username = :user");
              $stmt->bindParam(':user', $user, PDO::PARAM_STR);
              $user = $value;
              $stmt->execute();
              $result = $stmt->fetch(PDO::FETCH_ASSOC);
              if(empty($result["fld_staff_num"]) && !isset($result["fld_staff_num"])) {
                $vars[$key] = $value;
              } else {
                $msg .= "Error: Username already exist!<br />";
                $count_error++;
              }
          } catch (PDOException $e) {
              $msg .= "Error: " . $e->getMessage() . "<br />";
          }
        } else if (isset($value) && !empty($value)) {
            $vars[$key] = $value;
        } else {
            $msg .= "Error: Field '". $fieldName ."' is not filled in.<br />";
            $count_error++;
        }
    }

    // Display error messages if there's any error.
    if ($count_error > 0) {
        echo $msg;
        echo "$count_error error(s) detected.<br />";
        echo "<button class='btn' onclick='history.go(-1);'>Back </button>";
        die();
    }
  }

//Create
if (isset($_POST['create'])) {
    try {
        $stmt = $conn->prepare("INSERT INTO tbl_staffs_a155652_pt2(fld_staff_num, fld_staff_name, fld_staff_username, fld_staff_password, fld_staff_email) VALUES(:sid, :name, :susername, :spassword, :email)");

        $stmt->bindParam(':sid', $sid, PDO::PARAM_STR);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':susername', $susername, PDO::PARAM_STR);
        $stmt->bindParam(':spassword', $spassword, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);

        $sid = titleCase($vars['sid']);
        $susername = $vars['susername'];
        $spassword = password_hash($vars['spassword'], PASSWORD_DEFAULT);
        $email = $vars['email'];
        $name = titleCase($vars['name']);

        $stmt->execute();
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}

//Update
if (isset($_POST['update'])) {
    try {
        $stmt = $conn->prepare("UPDATE tbl_staffs_a155652_pt2 SET fld_staff_num = :sid, fld_staff_name = :name, fld_staff_username = :susername, fld_staff_password = :spassword, fld_staff_email = :email WHERE fld_staff_num = :oldsid");

        $stmt->bindParam(':sid', $sid, PDO::PARAM_STR);
        $stmt->bindParam(':name', $name, PDO::PARAM_STR);
        $stmt->bindParam(':oldsid', $oldsid, PDO::PARAM_STR);
        $stmt->bindParam(':susername', $susername, PDO::PARAM_STR);
        $stmt->bindParam(':spassword', $spassword, PDO::PARAM_STR);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);

        $oldsid = $_POST['oldsid'];
        $sid = titleCase($vars['sid']);
        $susername = $vars['susername'];
        $spassword = "";
        $email = $vars['email'];
        $name = titleCase($vars['name']);

        if(isset($_POST['newpass']) && !empty($_POST['newpass'])) {
          $spassword = password_hash($_POST['newpass'], PASSWORD_DEFAULT);
        } else {
          $spassword = password_hash($_POST['currpass'], PASSWORD_DEFAULT);
        }

        $stmt->execute();

        header("Location: staffs.php");
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}

//Delete
if (isset($_GET['delete'])) {
    try {
        $stmt = $conn->prepare("DELETE FROM tbl_staffs_a155652_pt2 where fld_staff_num = :sid");

        $stmt->bindParam(':sid', $sid, PDO::PARAM_STR);

        $sid = $_GET['delete'];

        $stmt->execute();

        header("Location: staffs.php");
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}

//Edit
if (isset($_GET['edit'])) {
    try {
        $stmt = $conn->prepare("SELECT * FROM tbl_staffs_a155652_pt2 where fld_staff_num = :sid");

        $stmt->bindParam(':sid', $sid, PDO::PARAM_STR);

        $sid = $_GET['edit'];

        $stmt->execute();

        $editrow = $stmt->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
}

  $conn = null;
