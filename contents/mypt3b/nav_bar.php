<nav class="navbar navbar-expand-md navbar-dark fixed-top">
<!-- <div class="container"> -->
    <a href="./home.php" class="navbar-brand">Antique Delights</a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarData" aria-controls="navbarData" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarData">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="./catalog.php" class="nav-link">Browse Catalog</a>
            </li>
            <?php if(isset($_SESSION['login_user'])) { ?>

            <?php } ?>
        </ul>
        <ul class="navbar-nav navbar-right">
            <li class="nav-item dropdown">
              <?php if(!isset($_SESSION['login_user'])) { ?>
                <a href="#" class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Login</a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <div class="container-fluid ">
                    <form action="login.php" method="post">
                      <div class="form-group">
                        <input class="form-control" name="lusername" type="text" placeholder="Username">
                      </div>
                      <div class="form-group">
                        <input class="form-control" name="lpassword" type="password" placeholder="Password">
                      </div>
                      <button type="submit" value="login" name="login" class="btn btn-primary">Login</button>
                    </form>
                  </div>
                </div>
              <?php } else { ?>
                <a href="#" class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Welcome, <?php echo $name ?>!</a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a href="./products.php" class="dropdown-item">Manage Products</a>
                  <a href="./customers.php" class="dropdown-item">Manage Customers</a>
                  <a href="./staffs.php" class="dropdown-item">Manage Staffs</a>
                  <a href="./orders.php" class="dropdown-item">Manage Orders</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="logout.php">Logout</a>
                </div>
              <?php } ?>
            </li>
        </ul>
    </div>
<!-- </div> -->

</nav>
<div style="min-height: 4em;"></div>
