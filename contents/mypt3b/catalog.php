<?php

	include_once('session.php');

	$total_pages = 0;
	$per_page = 4;

	if (isset($_GET["page"])){
		$page = intval($_GET["page"]);
	} else {
		$page = 1;
	}

	if(isset($_GET['sortby'])) {
		$sortby = $_GET['sortby'];
	} else {
		$_GET['sortby'] = "nameAsc";
		$sortby = $_GET['sortby'];
	}

	$calc = $per_page * $page;
	$start_from = $calc - $per_page;

?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once('head.php'); ?>
</head>

<body>
  <?php include_once('nav_bar.php'); ?>

  <div class="container">

    <div class="row">
			<!-- Catalog -->
      <div class="col">
        <div class="card">
          <div class="card-body">
            <h1 class="card-title display-4">Catalog</h1>
            <h6 class="card-subtitle mb-2 text-muted">View all our products here.</h6>
          </div>
					<ul class="list-group list-group-flush">
						<li class="list-group-item">
							<form action="catalog.php" method="get">
								<div class="form-row">
									<div class="input-group col-lg-4">
										<span class="input-group-btn">
											<button class="btn btn-secondary" type="submit">Period</button>
										</span>
										<select name="period" id="iperiod" class="form-control" required>
												<?php
												// Read
												try {
													$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
													$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
													$stmt = $conn->prepare("SELECT * FROM tbl_periods_a155652_pt2");
													$stmt->execute();
													$result = $stmt->fetchAll();
												} catch (PDOException $e) {
													echo "Error: " . $e->getMessage();
												}

												foreach ($result as $readrow) {
													echo "<option value='".$readrow['fld_period_num']."'";
													if (isset($_GET['period'])) {
														if ($_GET['period']==$readrow['fld_period_num']) {
															echo "selected";
														}
													}
													echo ">";
													echo $readrow['fld_period_name'];
													echo "</option>";
												}
												?>
											</select>
									</div>
									<div class="input-group col-lg-4">
										<span class="input-group-btn">
											<button class="btn btn-secondary" type="submit">Material</button>
										</span>
										<select class="form-control" name="material">
											<?php
												// Read
												try {
													$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
													$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
													$stmt = $conn->prepare("SELECT * FROM tbl_materials_a155652_pt2");
													$stmt->execute();
													$result = $stmt->fetchAll();
												} catch (PDOException $e) {
													echo "Error: " . $e->getMessage();
												}

												foreach ($result as $readrow) {
													echo "<option value='".$readrow['fld_material_num']."'";
													if (isset($_GET['material'])) {
														if ($_GET['material']==$readrow['fld_material_num']) {
														echo "selected";
														}
													}
													echo ">";
													echo $readrow['fld_material_name'];
													echo "</option>";
												}
											?>
										</select>
									</div>
									<div class="input-group col-lg-4">
										<span class="input-group-btn">
											<button class="btn btn-secondary" type="submit">Sort by</button>
										</span>
										<select class="form-control" name="sortby">
											<option <?php if($_GET['sortby'] == "nameAsc") echo "selected"; ?> value="nameAsc">Name (A-Z)</option>
											<option <?php if($_GET['sortby'] == "nameDesc") echo "selected"; ?> value="nameDesc">Name (Z-A)</option>
											<option <?php if($_GET['sortby'] == "priceAsc") echo "selected"; ?> value="priceAsc">Price (ascending)</option>
											<option <?php if($_GET['sortby'] == "priceDesc") echo "selected"; ?> value="priceDesc">Price (descending)</option>
										</select>
									</div>
								</div>
							</form>
						</li>
						<li class="list-group-item">
							<?php
								$filter = "";
								if(isset($_GET['material'])) {
									$material = $_GET['material'];
									$filter .= " AND tbl_products_a155652_pt2.fld_material LIKE '$material' ";
								}

								if(isset($_GET['period'])) {
									$period = $_GET['period'];
									$filter .= " AND tbl_products_a155652_pt2.fld_period LIKE '$period' ";
								}


								$sort = "";
								switch($sortby) {
									case "priceAsc":
										$sort .= " fld_product_price ASC ";
										break;
									case "priceDesc":
										$sort .= " fld_product_price DESC ";
										break;
									case "nameAsc":
										$sort .= " fld_product_name ASC ";
										break;
									case "nameDesc":
										$sort .= " fld_product_name DESC ";
										break;
								}

                try {
                  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                  $stmt = $conn->prepare("SELECT * FROM tbl_products_a155652_pt2, tbl_periods_a155652_pt2, tbl_materials_a155652_pt2 WHERE tbl_products_a155652_pt2.fld_material = tbl_materials_a155652_pt2.fld_material_num AND tbl_products_a155652_pt2.fld_period = tbl_periods_a155652_pt2.fld_period_num $filter ORDER BY $sort LIMIT $start_from, $per_page ");
                  $stmt->execute();
                  $result = $stmt->fetchAll();
                } catch (PDOException $e) {
                  echo "Error: " . $e->getMessage();
                }

								$i = 2;
								foreach ($result as $readrow) {
									if($i == 2) {
										echo "<div class='row' style='margin-bottom: 2em;'>";
									}?>
									<div class="col-sm-6 col-lg-6">
										<div class="card h-100">
											<ul class="list-group list-group-flush">
												<li class="list-group-item">
													<ul class="list-inline">
														<li class="list-inline-item"><a href="catalog.php?filter=true&period=<?php echo $readrow['fld_period'] ?>" class="badge badge-warning"><?php echo $readrow['fld_period_name'] ?></a></li>
														<li class="list-inline-item"><a href="catalog.php?filter=true&material=<?php echo $readrow['fld_material'] ?>" class="badge badge-success"><?php echo $readrow['fld_material_name'] ?></a></li>
														<li class="list-inline-item float-right" style="color: #444444;"><b>RM <?php echo $readrow['fld_product_price'] ?></b></li>
													</ul>
												</li>
											</ul>
											<img style="height: 320px; width: auto; object-fit: cover;" class="card-img-top" src="assets/products/<?php echo $readrow['fld_product_num'] ?>.jpg" alt="<?php echo $readrow['fld_product_name'] ?>">
											<div class="card-body">
												<h4 class="card-title"><?php echo $readrow['fld_product_name'] ?></h4>
												<p class="card-text" style="color: #7a7a7a;"><em><?php echo $readrow['fld_description'] ?></em></p>
											</div>
											<ul class="list-group list-group-flush">
												<li class="list-group-item">
													<a href="product_details.php?pid=<?php echo $readrow['fld_product_num']; ?>" class="btn btn-primary btn-block">View Product</a>
												</li>
											</ul>

										</div>
									</div>
								<?php if($i == 1) { echo "</div>"; } $i--; if($i == 0) {$i = 2;} } ?>
						</li>
						<li class="list-group-item">
							<?php
								$total_pages = 0;
								try {
                  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

										$sort = "";
										switch($sortby) {
											case "priceAsc":
												$sort = "fld_product_price ASC ";
												break;
											case "priceDesc":
												$sort = "fld_product_price DESC ";
												break;
											case "nameAsc":
												$sort = "fld_product_name ASC ";
												break;
											case "nameDesc":
												$sort = "fld_product_name DESC ";
												break;
										}

									$filter = "";
									if(isset($_GET['material']) && isset($_GET['period'])) {
										$material = $_GET['material'];
										$period = $_GET['period'];
										$filter .= " WHERE tbl_products_a155652_pt2.fld_material LIKE '$material' AND tbl_products_a155652_pt2.fld_period LIKE '$period' ";
									} else if(isset($_GET['material'])) {
										$material = $_GET['material'];
										$filter .= " WHERE tbl_products_a155652_pt2.fld_material LIKE '$material' ";
									} else if(isset($_GET['period'])) {
										$period = $_GET['period'];
										$filter .= " WHERE tbl_products_a155652_pt2.fld_period LIKE '$period' ";
									}

									$stmt = $conn->prepare("SELECT COUNT(*) AS total FROM tbl_products_a155652_pt2 $filter ORDER BY $sort");


                  $stmt->execute();
                  $result = $stmt->fetchAll();
									$total_records = $result[0]['total'];
									$total_pages = ceil($total_records / $per_page);
                } catch (PDOException $e) {
                  echo "Error: " . $e->getMessage();
                }
							?>
							<nav aria-label="Page navigation example">
							  <ul class="pagination justify-content-end">
									<?php if ($page == 1) { ?> <li class="page-item disabled"> <?php } else { ?> <li class="page-item"> <?php } ?>
										<?php
											$urlFilter = "";
											if(isset($_GET['material'])) {
												$urlFilter .= "&material=".$material;
											}

											if(isset($_GET['period'])) {
												$urlFilter .= "&period=".$period;
											}

											switch($sortby) {
												case "priceAsc":
													$urlSort = "&sortby=priceAsc";
													break;
												case "priceDesc":
													$urlSort = "&sortby=priceDesc";
													break;
												case "nameAsc":
													$urlSort = "&sortby=nameAsc";
													break;
												case "nameDesc":
													$urlSort = "&sortby=nameDesc";
													break;
											}
										?>
							      <a class="page-link" href="catalog.php?page=<?php echo $page-1; echo $urlFilter; echo $urlSort; ?>" tabindex="-1">Previous</a>
							    </li>
									<?php
										for ($i=1; $i<=$total_pages; $i++) {
											if ($i == $page) {
												echo '<li class="page-item active"><a class="page-link" href="catalog.php?page='.$i.'">'.$i.'</a></li>';
											} else {
												echo '<li class="page-item"><a class="page-link" href="catalog.php?page='.$i.'">'.$i.'</a></li>';
											}
										}
									?>
									<?php if ($page == $total_pages) { ?> <li class="page-item disabled"> <?php } else { ?> <li class="page-item"> <?php } ?>
							      <a class="page-link" href="catalog.php?page=<?php echo $page+1; echo $urlFilter; echo $urlSort; ?>" tabindex="-1">Next</a>
							    </li>
							  </ul>
							</nav>
						</li>
					</ul>
        </div>
      </div>
    </div>
  </div>

  <?php include_once('footer.php'); ?>
  <?php include_once('bootstrap_js.php'); ?>

</body>

</html>
