<?php

    function titleCase($s)
    {
        $s = ucwords(strtolower($s));
        $start = intval(strpos($s, stristr($s, "(")));

        if ($start <> 0) {
            $end = intval(strpos($s, stristr($s, ")")));
            $result = substr($s, ($start+1), (($end - $start)-1));
            $s = str_replace($result, strtoupper($result), $s);
        }

        return $s;
    }

    // return string with first letters of sentences capitalized
function ucsentence($str) {
  if ($str) { // input
    $str = preg_replace('/'.chr(32).chr(32).'+/', chr(32), $str); // recursively replaces all double spaces with a space
    if (($x = substr($str, 0, 10)) && ($x == strtoupper($x))) $str = strtolower($str); // sample of first 10 chars is ALLCAPS so convert $str to lowercase; if always done then any proper capitals would be lost
    $na = array('. ', '! ', '? '); // punctuation needles
    foreach ($na as $n) { // each punctuation needle
      if (strpos($str, $n) !== false) { // punctuation needle found
        $sa = explode($n, $str); // split
        foreach ($sa as $s) $ca[] = ucfirst($s); // capitalize
        $str = implode($n, $ca); // replace $str with rebuilt version
        unset($ca); //  clear for next loop
      }
    }
    return ucfirst(trim($str)); // capitalize first letter in case no punctuation needles found
  }
}

    include_once 'database.php';

    $conn = new PDO("mysql:host=$servername; dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if (isset($_POST['create']) || isset($_POST['update'])) {

        /**
        *   Initialise variables.
        *
        *   Use array because it will be much easier to
        *   manage and used together with foreach.
        */
        $vars = array(
            "pid" => "",
            "name" => "",
            "price" => "",
            "material" => "",
            "period" => "",
            "description" => "",
            "quantity" => ""
        );

        $count_error = 0;
        $msg = "";

        /**
        *    Validate submitted data.
        *
        *   If empty show error message,
        *   else put in local variables.
        */

        foreach ($_POST as $key=>$value) {

            /**
            *   Set the fieldName.
            *
            *   To make the error messages prettier. It will
            *   use the field's title instead of the field's ID.
            */

            $fieldName = "";
            switch ($key) {
                case "pid":
                    $fieldName = "Product ID";
                    break;
                case "name":
                    $fieldName = "Product Name";
                    break;
                case "price":
                    $fieldName = "Price";
                    break;
                case "material":
                    $fieldName = "Material";
                    break;
                case "period":
                    $fieldName = "Period";
                    break;
                case "quantity":
                    $fieldName = "Quantity";
                    break;
                case "description":
                    $fieldName = "Description";
                    break;
                default:
                    $fieldName = $key;
                    break;
            }

            // Check if the value is set and not empty
            if ($key == "pid"){
              try {
                  $stmt = $conn->prepare("SELECT fld_product_num FROM tbl_products_a155652_pt2 WHERE fld_product_num = :findpid");
                  $stmt->bindParam(':findpid', $findpid, PDO::PARAM_STR);
                  $findpid = $value;
                  $stmt->execute();
                  $result = $stmt->fetch(PDO::FETCH_ASSOC);
                  if(empty($result["fld_product_num"]) && !isset($result["fld_product_num"])) {
                    $vars[$key] = $value;
                  } else if(isset($_POST['update'])) {
                    if($_POST['oldpid'] == $result["fld_product_num"]) {
                      $vars[$key] = $value;
                    }
                  } else {
                    $msg .= "Error: Product ID already exist!<br />";
                    $count_error++;
                  }
              } catch (PDOException $e) {
                  $msg .= "Error: " . $e->getMessage() . "<br />";
              }
            } else if (isset($value) && !empty($value)) {
                $vars[$key] = $value;
            } else if ($key == "price") {
              if($value == "0") {
                $vars[$key] = "0.00";
              }
            } else if ($key == "quantity") {
              if($value == "0") {
                $vars[$key] = $value;
              }
            } else {
                $msg .= "Error: Field '". $fieldName ."' is not filled in.<br />";
                $count_error++;
            }
        }

        // Display error messages if there's any error.
        if ($count_error > 0) {
            echo $msg;
            echo "$count_error error(s) detected.<br />";
            echo "<button class='btn' onclick='history.go(-1);'>Back </button>";
            die();
        }
      }

    // Create
    if (isset($_POST['create'])) {
        try {
            $stmt = $conn->prepare("INSERT INTO tbl_products_a155652_pt2(fld_product_num, fld_product_name, fld_product_price, fld_material, fld_period, fld_quantity, fld_description) VALUES(:pid, :name, :price, :material, :period, :quantity, :description)");

            $stmt->bindParam(':pid', $pid, PDO::PARAM_STR);
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->bindParam(':price', $price, PDO::PARAM_INT);
            $stmt->bindParam(':material', $material, PDO::PARAM_STR);
            $stmt->bindParam(':period', $period, PDO::PARAM_STR);
            $stmt->bindParam(':quantity', $quantity, PDO::PARAM_INT);
            $stmt->bindParam(':quantity', $quantity, PDO::PARAM_INT);
            $stmt->bindParam(':description', $description, PDO::PARAM_INT);

            $pid = $vars['pid'];
            $name = titleCase($vars['name']);
            $price = $vars['price'];
            $material =  $vars['material'];
            $period = $vars['period'];
            $quantity = $vars['quantity'];
            $description = ucsentence($vars['description']);

            $stmt->execute();
        } catch (PDOException $e) {
            echo "ERROR: " . $e->getMessage();
        }
    }

    //Update
    if (isset($_POST['update'])) {
        try {
            $stmt = $conn->prepare("UPDATE tbl_products_a155652_pt2 SET fld_product_num = :pid,
            fld_product_name = :name, fld_product_price = :price, fld_material = :material,
            fld_period = :period, fld_quantity = :quantity, fld_description = :description
            WHERE fld_product_num = :oldpid");

            $stmt->bindParam(':pid', $pid, PDO::PARAM_STR);
            $stmt->bindParam(':name', $name, PDO::PARAM_STR);
            $stmt->bindParam(':price', $price, PDO::PARAM_INT);
            $stmt->bindParam(':material', $material, PDO::PARAM_STR);
            $stmt->bindParam(':period', $period, PDO::PARAM_STR);
            $stmt->bindParam(':quantity', $quantity, PDO::PARAM_INT);
            $stmt->bindParam(':oldpid', $oldpid, PDO::PARAM_STR);
            $stmt->bindParam(':description', $description, PDO::PARAM_STR);

            $pid = $vars['pid'];
            $name = titleCase($vars['name']);
            $price = $vars['price'];
            $material =  $vars['material'];
            $period = $vars['period'];
            $quantity = $vars['quantity'];
            $oldpid = $_POST['oldpid'];
            $description = ucsentence($vars['description']);

            $stmt->execute();

            header("Location: products.php");
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    //Delete
    if (isset($_GET['delete'])) {
        try {
            $stmt = $conn->prepare("DELETE FROM tbl_products_a155652_pt2 WHERE fld_product_num = :pid");

            $stmt->bindParam(':pid', $pid, PDO::PARAM_STR);

            $pid = $_GET['delete'];

            $stmt->execute();

            header("Location: products.php");
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    //Edit
    if (isset($_GET['edit'])) {
        try {
            $stmt = $conn->prepare("SELECT * FROM tbl_products_a155652_pt2 WHERE fld_product_num = :pid");

            $stmt->bindParam(':pid', $pid, PDO::PARAM_STR);

            $pid = $_GET['edit'];

            $stmt->execute();

            $editrow = $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    $conn = null;
