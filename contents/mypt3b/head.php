<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Hakim Zulkufli">
<link rel="icon" href="./favicon.png">

<title>Antiques Delight</title>

<!-- Bootstrap core CSS -->
<link href="./css/bootstrap.min.css" rel="stylesheet">
<link href="./open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">

<style>
    body {

        width: 100%;
        height: 100%;
        background-image: url('bg2.png');
        background-size: cover;
        background-repeat: no-repeat;
        background-position: right center;
        background-attachment: fixed;
        color: #153412;
        background-color: #F4EDD5;
        margin-bottom: 80px;
    }

    .card {
        margin-top: 1em;
        /* box-shadow: 0px 0px 2px 0px #292E32; */
        background: linear-gradient(rgba(255, 255, 255, 0.9), rgba(255, 255, 255, 0.9));
        border-radius: 0px;
    }

    .navbar {
        background: linear-gradient(rgba(52, 58, 64, 0.9), rgba(52, 58, 64, 0.9));
        /* background: linear-gradient(rgba(255, 255, 255, 0.9), rgba(255, 255, 255, 0.9)) !important; */
    }

    .container {
    }

    .footer {
        position: fixed;
        color: white;
        bottom: 0;
        width: 100%;
        height: 60px; /* Set the fixed height of the footer here */
        line-height: 60px; /* Vertically center the text there */
        background: linear-gradient(rgba(52, 58, 64, 0.9), rgba(52, 58, 64, 0.9));
    }
</style>
