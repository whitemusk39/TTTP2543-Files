<?php
    $faculties = array(
        array(
                "name" => "Faculty of Information Science and Technology",
                "abb" => "FTSM"
            ),
        array(
                "name" => "Faculty of Social Sciences and Humanities",
                "abb" => "FSSK"
            ),
        array(
                "name" => "Faculty of Pharmacy",
                "abb" => "FFAR"
            ),
        array(
                "name" => "Faculty of Engineering and Built Environment",
                "abb" => "FKAB"
            ),
        array(
                "name" => "Faculty of Education",
                "abb" => "FPEND"
            ),
        array(
                "name" => "Faculty of Science and Technology",
                "abb" => "FST"
            ),
        array(
                "name" => "Faculty of Islamic Studies",
                "abb" => "FPI"
            ),
        array(
                "name" => "Faculty of Law",
                "abb" => "FUU"
            ),
        array(
                "name" => "Faculty of Medicine",
                "abb" => "FPER"
            ),
        array(
                "name" => "Faculty of Economics and Management",
                "abb" => "FEP"
            ),
        array(
                "name" => "Faculty of Health Sciences",
                "abb" => "FSK"
            ),
        array(
                "name" => "Faculty of Dentistry",
                "abb" => "FPERG"
            ),
        array(
                "name" => "UKM-GSB Graduate School of Business",
                "abb" => "GSB"
            )
    );
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
        <div>
            <form action="http://lrgs.ftsm.ukm.my/users/mfn/assignment1/process.php" method="post">
            <table class="borang">
                <tr>
                    <th style="width: 115px" class="ada-border"><img src="./img/logo.png" alt="Universiti Kebangsaan Malaysia" width="100%" height="auto"></th>
                    <th class="u-center ada-border" style="background-color: #ebebeb"><h1>Student Web Hosting Registration</h1></th>
                </tr>
                <tr style="height: 30px"></tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">Name:</td>
                    <td><input required type="text" name="name" style="width: 100%"></td>
                </tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">Faculty:</td>
                    <td>
                        <select name="fac" style="width: 100.5%">
                            <option selected></option>
                            <?php
                                foreach ($faculties as $f) {
                                    echo '<option value="'.$f['abb'].'">'.$f['name'].'</option>';
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">Matric Number:</td>
                    <td><input required type="text" name="matricnum" pattern="^[a|A]\d{6}" style="width: 25%"></td>
                </tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">Siswa Email:</td>
                    <td><input required type="text" name="sis_email" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@(siswa.ukm.edu.my)" style="width: 40%">&nbsp;&nbsp;&nbsp;<span style="font-style: italic">E.g. ahmad@siswa.ukm.edu.my</span></td>
                </tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">Category:<br />(tick one only)</td>
                    <td>
                        <input required type="radio" name="cat" value="personal">&nbsp;Personal Use<br />
                        <input required type="radio" name="cat" value="club">&nbsp;Club/Association<br />
                        <input required type="radio" name="cat" value="workshop">&nbsp;Workshop/Seminar<br />
                        <input required type="radio" name="cat" value="research">&nbsp;Research<br />
                        <input required type="radio" name="cat" value="residential">&nbsp;Residential College<br />
                    </td>
                </tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">Web ID:</td>
                    <td><input required type="text" name="id" maxlength="10" style="width: 40%">&nbsp;&nbsp;&nbsp;<span style="font-style: italic">Max 10 characters<br/>E.g. If Web ID = ahmad, your webpage URL will be = http://www.ukm.my/ahmad/</span></td>
                </tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">Password:</td>
                    <td><input required type="password" name="password" minlength="6" style="width: 40%">&nbsp;&nbsp;&nbsp;<span style="font-style: italic">Min 6 characters</span></td>
                </tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">Retype Password:</td>
                    <td><input required type="password" name="password_2" minlength="6" style="width: 40%"></td>
                </tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">Usage Start Date:</td>
                    <td><input required type="date" name="begin" style="width: 25%"></td>
                </tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">Script:<br />(tick any)</td>
                    <td>
                        <input required type="checkbox" name="html" value="on" checked>&nbsp;HTML<br />
                        <input type="checkbox" name="asp" value="on">&nbsp;ASP.Net<br />
                        <input type="checkbox" name="jsp" value="on">&nbsp;JSP/Servlet<br />
                        <input type="checkbox" name="php" value="on">&nbsp;PHP<br />
                    </td>
                </tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">IP Address:</td>
                    <td><input required type="text" name="ip_add" pattern="^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$" style="width: 40%">&nbsp;&nbsp;&nbsp;<span style="font-style: italic">Format: xxx.xxx.xxx.xxx</span></td>
                </tr>
                <tr style="margin-top: 150px">
                    <td style="padding-left: 10px">Description:</td>
                    <td><textarea name="description" rows="7" style="width: 100%"></textarea></span></td>
                </tr>
                <tr>
                    <td>
                        <input type="reset" value="Reset">
                        <input type="submit" name="web_form" value="Submit">
                    </td>
                </tr>
            </table>
        </form>
        </div>
    </body>
</html>