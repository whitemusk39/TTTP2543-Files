<?php

  if (isset($_POST['biodata_validate'])) {

      // Connection variables
      $servername = "lrgs.ftsm.ukm.my";
      $username = "a155652";
      $password = "6Mw#6Um";
      $dbname = "a155652";

      /**
      *   Initialise variables.
      *
      *   Use array because it will be much easier to
      *   manage and used together with foreach.
      */

      $vars = array(
          "name" => "",
          "age" => "",
          "sex" => "",
          "address" => "",
          "email" => "",
          "dob" => "",
          "height" => "",
          "phone" => "",
          "color" => "",
          "fbtwig" => "",
          "univ" => ""
      );

      // Fill in the variables with their values
      foreach ($_POST as $key=>$value) {
          if ($key !== "biodata_validate" && $key !== "matricnum") {
              if (isset($value) && !empty($value)) {
                  $vars[$key] = $value;
              } else {
                  $msg .= "Error: Field '". $fieldName ."' is not filled in.<br />";
                  $count_error++;
              }
          }
      }

      echo "<pre>".htmlentities(print_r($vars))."</pre>";

      try {
          $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

          // Prepare SQL statement
          $stmt = $conn->prepare("INSERT INTO biodata(name, age, sex, address, email, dob, height, phone, color, fbtwig, univ) VALUES (:name, :age, :sex, :address, :email, :dob, :height, :phone, :color, :fbtwig, :univ)");

          // Bind the parameters
          // $stmt->bindParam(':name', $vars['name'], PDO::PARAM_STR);
          // $stmt->bindParam(':age', $vars['age'], PDO::PARAM_INT);
          // $stmt->bindParam(':sex', $vars['sex'], PDO::PARAM_STR);
          // $stmt->bindParam(':address', $vars['address'], PDO::PARAM_STR);
          // $stmt->bindParam(':email', $vars['email'], PDO::PARAM_STR);
          // $stmt->bindParam(':dob', $vars['dob'], PDO::PARAM_STR);
          // $stmt->bindParam(':height', $vars['height'], PDO::PARAM_STR);
          // $stmt->bindParam(':phone', $vars['phone'], PDO::PARAM_STR);
          // $stmt->bindParam(':color', $vars['color'], PDO::PARAM_STR);
          // $stmt->bindParam(':fbtwig', $vars['fbtwig'], PDO::PARAM_STR);
          // $stmt->bindParam(':univ', $vars['univ'], PDO::PARAM_STR);

          foreach ($vars as $key => &$value) {
              // Check if value is not numeric
              if (!is_numeric($value)) {
                  $stmt->bindParam(':'.$key.'', $value, PDO::PARAM_STR);
              } else {
                  $stmt->bindParam(':'.$key.'', $value, PDO::PARAM_INT);
              }
          }

          // Insert a row
          $stmt->execute();
          echo "New records created successfully.";
      } catch (PDOException $e) {
          echo "Error: ". $e->getMessage();
      }

      $conn = null;
  } else {
      echo "ERROR: You executed the wrong PHP script. Please contact the web administrator.";
      die();
  }
