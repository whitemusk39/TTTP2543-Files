<?php
    if (isset($_POST['biodata_form'])) {

        /**
        *   Initialise variables.
        *
        *   Use array because it will be much easier to
        *   manage and used together with foreach.
        */
        $vars = array(
            "name" => "",
            "age" => "",
            "sex" => "",
            "address" => "",
            "email" => "",
            "dob" => "",
            "height" => "",
            "phone" => "",
            "color" => "",
            "fbtwig" => "",
            "univ" => "",
            "matricnum" => "",
        );

        $count_error = 0;
        $msg = "";

        /**
        *    Validate submitted data.
        *
        *   If empty show error message,
        *   else put in local variables.
        */

        foreach ($_POST as $key=>$value) {

            /**
            *   Set the fieldName.
            *
            *   To make the error messages prettier. It will
            *   use the field's title instead of the field's ID.
            */

            $fieldName = "";
            switch ($key) {
                case "name":
                    $fieldName = "Name";
                    break;
                case "age":
                    $fieldName = "Age";
                    break;
                case "sex":
                    $fieldName = "Sex";
                    break;
                case "address":
                    $fieldName = "Address";
                    break;
                case "email":
                    $fieldName = "Email";
                    break;
                case "dob":
                    $fieldName = "Date of Birth";
                    break;
                case "height":
                    $fieldName = "Height";
                    break;
                case "phone":
                    $fieldName = "Tel";
                    break;
                case "color":
                    $fieldName = "My favourite colour";
                    break;
                case "fbtwig":
                    $fieldName = "FB/TW/IG";
                    break;
                case "university":
                    $fieldName = "My University";
                    break;
                default:
                    $fieldName = $key;
                    break;
            }

            // Check if the value is set and not empty
            if (isset($value) && !empty($value)) {
                $vars[$key] = $value;
            } else {
                $msg .= "Error: Field '". $fieldName ."' is not filled in.<br />";
                $count_error++;
            }
        }

        // Display error messages if there's any error.
        if ($count_error > 0) {
            echo $msg;
            echo "$count_error error(s) detected.<br />";
            die();
        }
    } else {
        echo "ERROR: You executed the wrong PHP script. Please contact the web administrator.";
        die();
    }

    function titleCase($s)
    {
        $s = ucwords(strtolower($s));
        $start = intval(strpos($s, stristr($s, "(")));

        if ($start <> 0) {
            $end = intval(strpos($s, stristr($s, ")")));
            $result = substr($s, ($start+1), (($end - $start)-1));
            $s = str_replace($result, strtoupper($result), $s);
        }

        return $s;
    }
?>

<!DOCTYPE html>
<html>
<head>
  <title>Biodata</title>
</head>
<body>
  <table border="1">
      <tr>
          <td><a href="./index.php">Biodata Form</a></td>
          <td><a href="./edit_bio.php">Edit Biodata</a></td>
          <td><a href="./lecturers_list.php">Lecturers List</a></td>
      </tr>
  </table>
  <hr>
<h1>Validate Biodata</h1>
<hr>
<form action="save_biodata.php" method="post">
  <table border="1" cellpadding="10">
    <tr>
      <td>Name:</td>
      <td><?php echo titleCase($vars['name']); ?><input type="hidden" name="name" value="<?php echo titleCase($vars['name']); ?>"></td>
    </tr>
    <tr>
      <td>Age:</td>
      <td><?php echo $vars['age']; ?><input type="hidden" name="age" value="<?php echo $vars['age']; ?>"></td>
    </tr>
    <tr>
      <td>Sex:</td>
      <td><?php echo titleCase($vars['sex']); ?><input type="hidden" name="sex" value="<?php echo titleCase($vars['sex']); ?>"></td>
    </tr>
    <tr>
      <td>Address:</td>
      <td><?php echo titleCase($vars['address']); ?><input type="hidden" name="address" value="<?php echo titleCase($vars['address']); ?>"></td>
    </tr>
    <tr>
      <td>Email:</td>
      <td><?php echo $vars['email']; ?><input type="hidden" name="email" value="<?php echo $vars['email']; ?>"></td>
    </tr>
    <tr>
      <td>Date of Birth:</td>
      <td><?php echo date("d-m-Y", strtotime($vars['dob'])); ?><input type="hidden" name="dob" value="<?php echo date("d-m-Y", strtotime($vars['dob'])); ?>"></td>
    </tr>
    <tr>
      <td>Height:</td>
      <td><?php echo $vars['height']; ?><input type="hidden" name="height" value="<?php echo $vars['height']; ?>"></td>
    </tr>
    <tr>
      <td>Tel:</td>
      <td><?php echo $vars['phone']; ?><input type="hidden" name="phone" value="<?php echo $vars['phone']; ?>"></td>
    </tr>
    <tr>
      <td>My Favorite Color:</td>
      <td><?php echo $vars['color']; ?><input type="hidden" name="color" value="<?php echo $vars['color']; ?>"></td>
    </tr>
    <tr>
      <td>Fb/TW/IG:</td>
      <td><?php echo $vars['fbtwig']; ?><input type="hidden" name="fbtwig" value="<?php echo $vars['fbtwig']; ?>"></td>
    </tr>
    <tr>
      <td>My University:</td>
      <td><?php echo $vars['university']; ?><input type="hidden" name="univ" value="<?php echo $vars['university']; ?>"></td>
    </tr>
    <tr>
      <td>My Matric Number:</td>
      <td><?php echo titleCase($vars['matricnum']); ?></td>
    </tr>
  </table>
<hr>
<input type="hidden" name="matricnum" value="<?php echo titleCase($vars['matricnum']); ?>">
<input type="submit" name="biodata_validate" value="Save My Biodata">
</form>

</body>
</html>
