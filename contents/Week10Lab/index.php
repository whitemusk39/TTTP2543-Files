<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Week10Lab -- A155652</title>
</head>
<body>
  <h1>Week10Lab -- A155652</h1>
  <h3>List of Files</h3>
  <?php

  $dir = './';

  $files = array();
  if (is_dir($dir)) {
  	if ($dh = opendir($dir)) {
  		while (($file = readdir($dh)) !== false) {
  			if (($file != '.')) {
  				$files[] = $file;
  			}
  		}
  		closedir($dh);
  	}
  }
  sort($files);

  foreach ($files as $file) {
    if($file != "index.php") {
      echo '<div><a href="'.$file.'">'.$file.'</a></div>';
    }
  }

  ?>
</body>
</html>
