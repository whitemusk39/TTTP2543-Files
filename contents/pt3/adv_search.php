<?php

	include_once('session.php');
  verifySession();

  function getMaterialID($mat) {
    switch($mat) {

    }
  }

	$total_pages = 0;
	$per_page = 6;

	if (isset($_GET["page"])){
		$page = intval($_GET["page"]);
	} else {
		$_GET["page"] = 1;
		$page = $_GET["page"];
	}

	$calc = $per_page * $page;
	$start_from = $calc - $per_page;

	if(isset($_GET['sortby'])) {
		$sortby = $_GET['sortby'];
	} else {
		$_GET['sortby'] = "nameAsc";
		$sortby = $_GET['sortby'];
	}

	if(isset($_GET['query'])) {
		$_GET['query'] = htmlspecialchars($_GET['query']);
		$q = htmlspecialchars($_GET['query']);
		$qq = explode(" ", $q);
	}

?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once('head.php'); ?>
</head>

<body>
  <?php include_once('nav_bar.php'); ?>

  <div class="container">

    <div class="row">
      <div class="col">
        <div class="card">
          <div class="card-body">
            <h1 class="card-title display-4">Advanced Search</h1>
            <h6 class="card-subtitle mb-2 text-muted">Fine-tune your query.</h6>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">
              <form action="adv_search.php" method="get">
                <div class="form-row">
                  <div class="form-group col-md-3">
                    <label>Field</label>
                    <select class="form-control" name="field1" required style="margin-bottom: 1em;">
                      <option <?php if(isset($_GET['field1'])) { if($_GET['field1'] == "name") echo "selected"; } ?> value="name">Name</option>
                      <option <?php if(isset($_GET['field1'])) { if($_GET['field1'] == "desc") echo "selected"; } ?> value="desc">Description</option>
                      <option <?php if(isset($_GET['field1'])) { if($_GET['field1'] == "material") echo "selected"; } ?> value="material">Material</option>
                      <option <?php if(isset($_GET['field1'])) { if($_GET['field1'] == "period") echo "selected"; } ?> value="period">Period</option>
                      <option <?php if(isset($_GET['field1'])) { if($_GET['field1'] == "price") echo "selected"; } ?> value="price">Price</option>
                    </select>
                    <select class="form-control" name="field2" required style="margin-bottom: 1em;">
                      <option <?php if(isset($_GET['field2'])) { if($_GET['field2'] == "desc") echo "selected"; } ?> value="desc">Description</option>
                      <option <?php if(isset($_GET['field2'])) { if($_GET['field2'] == "name") echo "selected"; } ?> value="name">Name</option>
                      <option <?php if(isset($_GET['field2'])) { if($_GET['field2'] == "material") echo "selected"; } ?> value="material">Material</option>
                      <option <?php if(isset($_GET['field2'])) { if($_GET['field2'] == "period") echo "selected"; } ?> value="period">Period</option>
                      <option <?php if(isset($_GET['field2'])) { if($_GET['field2'] == "price") echo "selected"; } ?> value="price">Price</option>
                    </select>
                    <select class="form-control" name="field3" required>
                      <option <?php if(isset($_GET['field3'])) { if($_GET['field3'] == "name") echo "selected"; } ?> value="name">Name</option>
                      <option <?php if(isset($_GET['field3'])) { if($_GET['field3'] == "desc") echo "selected"; } ?> value="desc">Description</option>
                      <option <?php if(isset($_GET['field3'])) { if($_GET['field3'] == "material") echo "selected"; } ?> value="material">Material</option>
                      <option <?php if(isset($_GET['field3'])) { if($_GET['field3'] == "period") echo "selected"; } ?> value="period">Period</option>
                      <option <?php if(isset($_GET['field3'])) { if($_GET['field3'] == "price") echo "selected"; } ?> value="price">Price</option>
                    </select>
                  </div>
                  <div class="form-group col-md-7">
                    <label>Search Term</label>
                    <input type="text" name="term1" value="<?php if(isset($_GET['term1'])) echo $_GET['term1']; ?>" class="form-control" style="margin-bottom: 1em;" required>
                    <input type="text" name="term2" value="<?php if(isset($_GET['term2'])) echo $_GET['term2']; ?>" class="form-control" style="margin-bottom: 1em;" required>
                    <input type="text" name="term3" value="<?php if(isset($_GET['term3'])) echo $_GET['term3']; ?>" class="form-control" required>
                  </div>
                  <div class="form-group col-md-2">
                    <label>Operator</label>
                    <select class="form-control" name="operator1" required style="margin-bottom: 1em;">
                      <option <?php if(isset($_GET['operator1'])) { if($_GET['operator1'] == "and") echo "selected"; } ?> value="and">AND</option>
                      <option <?php if(isset($_GET['operator1'])) { if($_GET['operator1'] == "or") echo "selected"; } ?> value="or">OR</option>
                    </select>
                    <select class="form-control" name="operator2" required>
                      <option <?php if(isset($_GET['operator2'])) { if($_GET['operator2'] == "and") echo "selected"; } ?> value="and">AND</option>
                      <option <?php if(isset($_GET['operator2'])) { if($_GET['operator2'] == "or") echo "selected"; } ?> value="or">OR</option>
                    </select>
                  </div>
                </div>
                <button type="submit" name="advQuery" value="true" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-secondary">Clear</button>
              </form>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <?php if(isset($_GET['advQuery'])) { ?>
    <div class="row">
      <div class="col">
        <div class="card">
          <div class="card-body">
            <h1 class="card-title display-4">Catalog</h1>
            <h6 class="card-subtitle mb-2 text-muted">View all our products here.</h6>
          </div>
					<ul class="list-group list-group-flush">
						<li class="list-group-item">
							<?php

              $field1 = $_GET['field1'];
              $field2 = $_GET['field2'];
              $field3 = $_GET['field3'];

              $term1 = $_GET['term1'];
              $term2 = $_GET['term2'];
              $term3 = $_GET['term3'];

              $operator1 = $_GET['operator1'];
              $operator2 = $_GET['operator2'];

                $queryBuilder = " AND ";

                switch($field1) {
                  case "name":
                    $queryBuilder .= " fld_product_name LIKE '%$term1%' $operator1 ";
                    break;
                  case "description":
                    $queryBuilder .= " fld_description LIKE '%$term1%' $operator1 ";
                    break;
                  case "material":
                    $queryBuilder .= " fld_material LIKE '%$term1%' $operator1 ";
                    break;
                  case "period":
                    $queryBuilder .= " fld_period LIKE '%$term1%' $operator1 ";
                    break;
                  case "price":
                    $t1 = explode(" to ", $term1);
                    $queryBuilder .= " fld_product_price BETWEEN '$t1[0]' AND '$t1[1]' ";
                    break;
                }

                //$queryBuilder .= " $operator1 ";

                switch($field2) {
                  case "name":
                    $queryBuilder .= " fld_product_name LIKE '%$term2%' $operator2 ";
                    break;
                  case "description":
                    $queryBuilder .= " fld_description LIKE '%$term2%' $operator2 ";
                    break;
                  case "material":
                    $queryBuilder .= " fld_material LIKE '%$term2%' $operator2 ";
                    break;
                  case "period":
                    $queryBuilder .= " fld_period LIKE '%$term2%' $operator2 ";
                    break;
                  case "price":
                    $t2 = explode(" to ", $term2);
                    $queryBuilder .= " fld_product_price BETWEEN '$t2[0]' AND '$t2[1]' ";
                    break;
                }

                //$queryBuilder .= " $operator2 ";

                switch($field3) {
                  case "name":
                    $queryBuilder .= " fld_product_name LIKE '%$term3%' ";
                    break;
                  case "description":
                    $queryBuilder .= " fld_description LIKE '%$term3%' ";
                    break;
                  case "material":
                    $queryBuilder .= " fld_material LIKE '%$term3%' ";
                    break;
                  case "period":
                    $queryBuilder .= " fld_period LIKE '%$term3%' ";
                    break;
                  case "price":
                    $t3 = explode(" to ", $term3);
                    $queryBuilder .= " fld_product_price BETWEEN '$t3[0]' AND '$t3[1]' ";
                    break;
                }

                // echo $queryBuilder;

	                try {
	                  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	                  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	                  $stmt = $conn->prepare("SELECT DISTINCT * FROM tbl_products_a155652_pt2, tbl_periods_a155652_pt2, tbl_materials_a155652_pt2 WHERE tbl_products_a155652_pt2.fld_material = tbl_materials_a155652_pt2.fld_material_num AND tbl_products_a155652_pt2.fld_period = tbl_periods_a155652_pt2.fld_period_num $queryBuilder LIMIT $start_from, $per_page ");
	                  $stmt->execute();
	                  $result = $stmt->fetchAll();
	                } catch (PDOException $e) {
	                  echo "Error: " . $e->getMessage();
	                }

									if(isset($_GET['advQuery'])) {
										if(count($result ) == 0) {
											echo '<div class="alert alert-danger" role="alert" style="margin-top: 1em;">No products matched your description.</div>';
										}
									}

								$i = 2;
								foreach ($result as $readrow) {
									if($i == 2) {
										echo "<div class='row' style='margin-bottom: 2em;'>";
									}?>
									<div class="col-lg-6">
										<div class="card h-100">
											<ul class="list-group list-group-flush">
												<li class="list-group-item">
													<ul class="list-inline">
														<li class="list-inline-item"><a href="adv_search.php?filter=true&period=<?php echo $readrow['fld_period'] ?>" class="badge badge-warning"><?php echo $readrow['fld_period_name'] ?></a></li>
														<li class="list-inline-item"><a href="adv_search.php?filter=true&material=<?php echo $readrow['fld_material'] ?>" class="badge badge-success"><?php echo $readrow['fld_material_name'] ?></a></li>
														<li class="list-inline-item float-right" style="color: #444444;"><b>RM <?php echo $readrow['fld_product_price'] ?></b></li>
													</ul>
												</li>
											</ul>
											<img style="height: 320px; width: auto; object-fit: cover;" class="card-img-top" src="assets/products/<?php echo $readrow['fld_product_num'] ?>.jpg" alt="<?php echo $readrow['fld_product_name'] ?>">
											<div class="card-body">
												<h4 class="card-title"><?php echo $readrow['fld_product_name'] ?></h4>
												<p class="card-text" style="color: #7a7a7a;"><em><?php echo $readrow['fld_description'] ?></em></p>
											</div>
											<ul class="list-group list-group-flush">
												<li class="list-group-item">
													<a href="product_details.php?pid=<?php echo $readrow['fld_product_num']; ?>" class="btn btn-primary btn-block">View Product</a>
												</li>
											</ul>

										</div>
									</div>
								<?php if($i == 1) { echo "</div>"; } $i--; if($i == 0) {$i = 2;} } ?>
						</li>
						<li class="list-group-item">
							<?php
								$total_pages = 0;
                $urlGet = "&advQuery=true&field1=$field1&term1=".htmlspecialchars($term1)."&operator1=$operator1";
                $urlGet .= "&field2=$field2&term2=".htmlspecialchars($term2)."&operator2=$operator2";
                $urlGet .= "&field3=$field3&term3=".htmlspecialchars($term1);

								try {
                  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

									$stmt = $conn->prepare("SELECT * FROM tbl_products_a155652_pt2, tbl_periods_a155652_pt2, tbl_materials_a155652_pt2 WHERE tbl_products_a155652_pt2.fld_material = tbl_materials_a155652_pt2.fld_material_num AND tbl_products_a155652_pt2.fld_period = tbl_periods_a155652_pt2.fld_period_num $queryBuilder");

                  $stmt->execute();
                  $result = $stmt->fetchAll();
									$total_records = count($result);
									$total_pages = ceil($total_records / $per_page);
                } catch (PDOException $e) {
                  echo "Error: " . $e->getMessage();
                }
							?>
							<nav aria-label="Page navigation example">
							  <ul class="pagination justify-content-end">
									<?php if ($page == 1) { ?> <li class="page-item disabled"> <?php } else { ?> <li class="page-item"> <?php } ?>
							      <a class="page-link" href="adv_search.php?page=<?php echo $page-1; echo $urlGet; ?>" tabindex="-1">Previous</a>
							    </li>
									<?php
										$links = "";
										$current_page = $_GET['page'];

								    if($total_pages == 1) {
											$links .= '<li class="page-item active"><a class="page-link" href="adv_search.php?page=1">1</a></li>';
										} else if ($total_pages >= 1 && $current_page <= $total_pages) {
											if($current_page == 1) {
												$links .= '<li class="page-item active"><a class="page-link" href="adv_search.php?page=1'.$urlGet.'">1</a></li>';
											} else {
												$links .= '<li class="page-item"><a class="page-link" href="adv_search.php?page=1'.$urlGet.'">1</a></li>';
											}

								        $i = max(2, $current_page - 1);
								        if ($i > 2)
								            $links .= '<li class="page-item disabled"><a class="page-link">...</a></li>';
								        for (; $i < min($current_page + 2, $total_pages); $i++) {
													if($i == $current_page) {
														$links .= '<li class="page-item active"><a class="page-link" href="adv_search.php?page='.$i.''.$urlGet.'">'.$i.'</a></li>';
													} else {
														$links .= '<li class="page-item "><a class="page-link" href="adv_search.php?page='.$i.''.$urlGet.'">'.$i.'</a></li>';
													}
								        }
								        if ($i != $total_pages)
								            $links .= '<li class="page-item disabled"><a class="page-link">...</a></li>';
														if($current_page == $total_pages) {
															$links .= '<li class="page-item active"><a class="page-link" href="adv_search.php?page='.$total_pages.''.$urlGet.'">'.$total_pages.'</a></li>';
														} else {
															$links .= '<li class="page-item"><a class="page-link" href="adv_search.php?page='.$total_pages.''.$urlGet.'">'.$total_pages.'</a></li>';
														}

								    }

										echo $links;

									?>
									<?php if ($page == $total_pages || $total_pages == 0) { ?> <li class="page-item disabled"> <?php } else { ?> <li class="page-item"> <?php } ?>
							      <a class="page-link" href="adv_search.php?page=<?php echo $page+1; echo $urlGet; ?>" tabindex="-1">Next</a>
							    </li>
							  </ul>
							</nav>
						</li>
					</ul>
        </div>
      </div>
    </div>
  <?php } ?>
  </div>

  <?php include_once('footer.php'); ?>
  <?php include_once('bootstrap_js.php'); ?>

</body>

</html>
