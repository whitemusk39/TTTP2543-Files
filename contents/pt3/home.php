<?php include_once('session.php'); ?>
<!DOCTYPE html>
<html>

<head>
	<?php include_once('head.php'); ?>
</head>

<body>

	<?php include_once('nav_bar.php'); ?>

	<div class="container">
		<div class="row">
			<div class="offset-lg-2 col-lg-8">
				<div class="jumbo fixed-bottom">
					<!-- <img src="favicon.png" class="img-fluid" width="50%" height="auto" alt=""> -->
					<h1 class="display-4 txtshad align-middle" style="color: white;">"Live in the Past. Fashionably."</h1>
					<p class="display-6 txtshad" style="color: white; font-style: italic;">Dress up your interior with our huge collection of antique furnitures.</p>
					<?php if (!isset($_SESSION['login_user'])) {
						echo '<a href="login.php"><button type="button" class="btn btn-light display-4 shopnow">STAFF LOGIN</button></a>';
					} else {
					 	echo '<a href="catalog.php"><button type="button" class="btn btn-light display-4 shopnow">BROWSE CATALOG</button></a>';
					} ?>
					<?php include_once('footer.php'); ?>
				</div>
			</div>
		</div>
	</div>


	<?php include_once('bootstrap_js.php'); ?>

</body>

</html>
