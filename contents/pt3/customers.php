<?php
	include('session.php');
	verifySession();
?>
    <!DOCTYPE html>
    <html>

    <head>
        <?php include_once('head.php'); ?>
    </head>

    <body>
        <?php include_once('nav_bar.php'); ?>

        <div class="container">

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                          <?php
                          include_once 'customers_crud.php';
                          ?>
                            <h1 class="card-title display-4">Add a new Customer</h1>
                            <h6 class="card-subtitle mb-2 text-muted">Have a new customer? Add them here.</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <form action="customers.php" method="post">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label>Customer ID</label>
                                            <input class="form-control" name="cid" type="text" value="<?php if  (isset($_GET['edit'])) { echo $editrow['fld_customer_num']; } ?>" placeholder="Customer ID" style="text-transform: capitalize;">
                                        </div>
                                        <div class="form-group col-md-9">
                                            <label>Name</label>
                                            <input style="text-transform: capitalize;" class="form-control" name="name" type="text" value="<?php if (isset($_GET['edit'])) { echo $editrow['fld_customer_name']; } ?>" placeholder="Customer Name">
                                        </div>
                                    </div>
                                    <?php if (isset($_GET['edit'])) { ?>
                                        <input type="hidden" name="oldcid" value="<?php echo $editrow['fld_customer_num']; ?>">
                                        <button type="submit" value="create" name="update" class="btn btn-primary">Update</button>
                                    <?php } else { ?>
                                        <button type="submit" value="create" name="create" class="btn btn-primary">Create</button>
                                    <?php } ?>
                                        <button type="reset" class="btn">Clear</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title display-4">Customers</h1>
                            <h6 class="card-subtitle mb-2 text-muted">View all your customers here.</h6>
                        </div>
                        <table class="table table-hover table-bordered table-responsive" style="text-transform: capitalize; background-color: white; margin-bottom: 0;">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" width="5%">ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col" width="1%"></th>
                                    <th scope="col" width="1%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    // Read
                                    try {
                                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                        $stmt = $conn->prepare("SELECT * FROM tbl_customers_a155652_pt2");
                                        $stmt->execute();
                                        $result = $stmt->fetchAll();
                                    } catch (PDOException $e) {
                                        echo "Error: " . $e->getMessage();
                                    }
                                    foreach ($result as $readrow) {
                                        ?>
                                            <tr>
                                                <th scope="row">
                                                    <?php echo $readrow['fld_customer_num']; ?>
                                                </th>
                                                <td>
                                                    <?php echo $readrow['fld_customer_name']; ?>
                                                </td>
                                                <td style="text-align: center">
                                                    <a href="customers.php?edit=<?php echo $readrow['fld_customer_num']; ?>"><span class="oi oi-pencil" title="Edit" aria-hidden="true"></span></a>
                                                </td>
                                                <td style="text-align: center">
                                                    <a href="customers.php?delete=<?php echo $readrow['fld_customer_num']; ?>" onclick="return confirm('Are you sure to delete?');"><span class="oi oi-delete" title="Delete" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            <?php
                                    }
                                    $conn = null;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once('footer.php'); ?>
        <?php include_once('bootstrap_js.php'); ?>
    </body>

    </html>
