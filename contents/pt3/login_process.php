<?php
  session_start();

  $errBox = "<div class='row justify-content-md-center' style='margin-top: 1.5em;'><div class='col-md-8'><div class='alert alert-danger' role='alert'>Username or password is incorrect!</div></div></div>";
  $err = "";

  include("database.php");

  if(isset($_SESSION['login_user'])) {
    header("location: index.php");
  }

  if(isset($_POST['login'])) {
    // print_r($_POST);
   if((isset($_POST['lusername']) && !empty($_POST['lusername'])) && (isset($_POST['lpassword']) && !empty($_POST['lpassword']))) {
     try {
         $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
         $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         $stmt = $conn->prepare("SELECT fld_staff_num, fld_staff_password FROM tbl_staffs_a155652_pt2 WHERE fld_staff_username = :myusername");
         $stmt->bindParam(':myusername', $myusername, PDO::PARAM_STR);

         $myusername = $_POST['lusername'];

         $stmt->execute();
         $result = $stmt->fetch(PDO::FETCH_ASSOC);

         if(isset($result['fld_staff_num']) && !empty($result['fld_staff_num'])) {
           if(password_verify($_POST['lpassword'], $result['fld_staff_password'])) {
             $_SESSION['login_user'] = $myusername;
             header("location: index.php");
           } else {
             $err = $errBox;
           }
         } else {
           $err = $errBox;
         }
     } catch (PDOException $e) {
         echo "Error: " . $e->getMessage();
     }
   } else {
     $err = $errBox;
   }
  }
