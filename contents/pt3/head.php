<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="Hakim Zulkufli">
<link rel="icon" href="./favicon.png">

<title>Antique Delights</title>

<!-- Bootstrap core CSS -->
<link href="./css/bootstrap.min.css" rel="stylesheet">
<link href="./open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=News+Cycle" rel="stylesheet">

<style>

    html {
      min-height: 100%;
    }

    body {
        width: 100%;
        height: 100%;
        color: #153412;
        margin-bottom: 80px;
        background-image: url('bg2.png');
        background-size: cover;
        background-repeat: no-repeat;
        background-position: right center;
        background-attachment: fixed;
        background-color: #f4edd5;
    }

    .card {
        margin-top: 1em;
        /* box-shadow: 0px 0px 2px 0px #292E32; */
        background: linear-gradient(rgba(255, 255, 255, 0.9), rgba(255, 255, 255, 0.9));
        border-radius: 0px;
    }

    .jumbo {
      background: linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6));
      text-align: center;
      border-radius: 0;
      box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.6);
      padding-top: 1em;
      padding-bottom: 6em;
    }

    .txtshad {
      text-shadow: 1px 1px #444;
    }

    .fadebg {
      margin-top: 1em;

    }

    .navbar {
        background: linear-gradient(rgba(52, 58, 64, 0.9), rgba(52, 58, 64, 0.9));
        /* background: linear-gradient(rgba(255, 255, 255, 0.9), rgba(255, 255, 255, 0.9)) !important; */
    }

    .shopnow {
      background-color: rgba(0, 0, 0, 0.4);
      border-color: rgb(255, 255, 255);
      color: white;
      font-size: 2em;
      font-weight: bold;
      margin-top: 0.3em;
      text-shadow: 1px 1px #444;
    }

    .container {
    }

    .footer {
        position: fixed;
        color: white;
        bottom: 0;
        width: 100%;
        height: 60px; /* Set the fixed height of the footer here */
        line-height: 60px; /* Vertically center the text there */
        background: linear-gradient(rgba(52, 58, 64, 0.9), rgba(52, 58, 64, 0.9));
    }
</style>
