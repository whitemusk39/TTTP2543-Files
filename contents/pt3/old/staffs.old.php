<?php
include_once 'staffs_crud.php';
?>

	<!DOCTYPE html>
	<html>

	<head>
		<?php include_once('head.php'); ?>
	</head>

	<body>
	<?php include_once('nav_bar.php'); ?>
		<center>
			
			<form action="staffs.php" method="post">
				Staff ID
				<input name="sid" type="text" value="<?php if (isset($_GET['edit'])) {
    echo $editrow['fld_staff_num'];
} ?>">
				<br> Name
				<input name="name" type="text" value="<?php if (isset($_GET['edit'])) {
    echo $editrow['fld_staff_name'];
} ?>">
				<br>
				<?php if (isset($_GET['edit'])) {
    ?>
				<input type="hidden" name="oldsid" value="<?php echo $editrow['fld_staff_num']; ?>">
				<button type="submit" name="update">Update</button>
				<?php
} else {
        ?>
				<button type="submit" name="create">Create</button>
				<?php
    } ?>
				<button type="reset">Clear</button>
			</form>
			<hr>
			<table border="1">
				<tr>
					<td>Staff ID</td>
					<td>Name</td>
					<td></td>
				</tr>
				<?php
    // Read
    try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("SELECT * FROM tbl_staffs_a155652_pt2");
        $stmt->execute();
        $result = $stmt->fetchAll();
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
    }
    foreach ($result as $readrow) {
        ?>
					<tr>
						<td>
							<?php echo $readrow['fld_staff_num']; ?>
						</td>
						<td>
							<?php echo $readrow['fld_staff_name']; ?>
						</td>
						<td>
							<a href="staffs.php?edit=<?php echo $readrow['fld_staff_num']; ?>">Edit</a>
							<a href="staffs.php?delete=<?php echo $readrow['fld_staff_num']; ?>" onclick="return confirm('Are you sure to delete?');">Delete</a>
						</td>
					</tr>
					<?php
    }
    $conn = null;
    ?>
			</table>
		</center>

		<?php include_once('bootstrap_js.php'); ?>
		<?php include_once('footer.php'); ?>
	</body>

	</html>