<footer class="footer">
    <div class="container">
        <span class="text">Made with &nbsp;<span class="oi oi-heart" title="love" aria-hidden="true"></span>&nbsp; by Hakim Zulkufli.</span>
    </div>
</footer>