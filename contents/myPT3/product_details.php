<?php
	include_once('session.php');
?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once('head.php'); ?>
</head>

<body>
  <?php include_once('nav_bar.php'); ?>

  <div class="container">

    <div class="row">
      <div class="col">
        <?php

          include_once 'database.php';

          try {
            $pid = $_GET['pid'];

            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("SELECT * FROM tbl_products_a155652_pt2, tbl_periods_a155652_pt2, tbl_materials_a155652_pt2 WHERE tbl_products_a155652_pt2.fld_material = tbl_materials_a155652_pt2.fld_material_num AND tbl_products_a155652_pt2.fld_period = tbl_periods_a155652_pt2.fld_period_num AND fld_product_num = :pid");
            $stmt->bindParam(':pid', $pid, PDO::PARAM_STR);
            $stmt->execute();

            $readrow = $stmt->fetch(PDO::FETCH_ASSOC);
          }
          catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
          }

          $conn = null;

        ?>

        <div class="card">


          <div class="card-body">
            <div class="media">
              <img src="assets/products/<?php echo $readrow['fld_product_num'] ?>.jpg" width="428px" height="auto" class="mr-4 rounded align-self-center">
              <div class="media-body">
                <h1 class="card-title display-4" style="text-transform: capitalize;"><?php echo $readrow['fld_product_name'] ?></h1>
                <h6 class="card-subtitle mb-2 text-muted" style="text-transform: capitalize;"><?php echo $readrow['fld_description'] ?></h6>
                <table class="table table-hover table-bordered" style="text-transform: capitalize; background: white">
                  <tbody>
                    <tr>
                      <th scope="row" width="20%">Product ID</th>
                      <td><?php echo $readrow['fld_product_num'] ?></td>
                    </tr>
                    <tr>
                      <th scope="row" width="20%">Price</th>
                      <td>RM <?php echo $readrow['fld_product_price'] ?></td>
                    </tr>
                    <tr>
                      <th scope="row" width="20%">Material</th>
                      <td><?php echo $readrow['fld_material_name'] ?></td>
                    </tr>
                    <tr>
                      <th scope="row" width="20%">Period</th>
                      <td><?php echo $readrow['fld_period_name'] ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>

            </div>

          </div>

        </div>
      </div>
    </div>
  </div>

  <?php include_once('footer.php'); ?>
  <?php include_once('bootstrap_js.php'); ?>

</body>

</html>
