<?php
	include_once('session.php');
	verifySession();
?>

<!DOCTYPE html>
	<html>

	<head>
        <?php include_once('head.php'); ?>
    </head>

    <body>
        <?php include_once('nav_bar.php'); ?>

        <div class="container">

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
														<?php
															include_once 'staffs_crud.php';
														?>
                            <h1 class="card-title display-4">Add a new Staff</h1>
                            <h6 class="card-subtitle mb-2 text-muted">Have a new staff? Add them here.</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <form action="staffs.php" method="post">
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                            <label>Staff ID</label>
                                            <input class="form-control" name="sid" type="text" value="<?php if  (isset($_GET['edit'])) { echo $editrow['fld_staff_num']; } ?>" placeholder="Staff ID">
                                        </div>
                                        <div class="form-group col-md-10">
                                            <label>Name</label>
                                            <input class="form-control" name="name" type="text" value="<?php if (isset($_GET['edit'])) { echo $editrow['fld_staff_name']; } ?>" placeholder="Staff Name">
                                        </div>
                                    </div>
																		<div class="form-row">
																				<?php if (isset($_GET['edit'])) { ?>
																					<div class="form-group col-md-2">
	                                            <label>Username</label>
	                                            <input class="form-control" name="susername" type="text" value="<?php if  (isset($_GET['edit'])) { echo $editrow['fld_staff_username']; } ?>" placeholder="Username"	>
	                                        </div>
																					<div class="form-group col-md-4">
	                                            <label>Email</label>
	                                            <input class="form-control" name="email" type="email" value="<?php if (isset($_GET['edit'])) { echo $editrow['fld_staff_email']; } ?>" placeholder="staff@email.com">
	                                        </div>
																					<div class="form-group col-md-3">
	                                            <label>Current Password</label>
	                                            <input class="form-control" name="currpass" type="password" placeholder="Current Password">
	                                        </div>
																					<div class="form-group col-md-3">
	                                            <label>New Password</label>
	                                            <input class="form-control" name="newpass" type="password" placeholder="Leave blank if no change">
	                                        </div>
																				<?php } else { ?>
																					<div class="form-group col-md-4">
	                                            <label>Username</label>
	                                            <input class="form-control" name="susername" type="text" value="<?php if  (isset($_GET['edit'])) { echo $editrow['fld_staff_username']; } ?>" placeholder="Username"	>
	                                        </div>
																					<div class="form-group col-md-4">
	                                            <label>Email</label>
	                                            <input class="form-control" name="email" type="email" value="<?php if (isset($_GET['edit'])) { echo $editrow['fld_staff_email']; } ?>" placeholder="staff@email.com">
	                                        </div>
																					<div class="form-group col-md-4">
	                                            <label>Password</label>
	                                            <input class="form-control" name="spassword" type="password" placeholder="Password">
	                                        </div>
																				<?php } ?>
                                    </div>
                                    <?php if (isset($_GET['edit'])) { ?>
                                        <input type="hidden" name="oldsid" value="<?php echo $editrow['fld_staff_num']; ?>">
                                        <button type="submit" value="update" name="update" class="btn btn-primary">Update</button>
                                    <?php } else { ?>
                                        <button type="submit" value="create" name="create" class="btn btn-primary">Create</button>
                                    <?php } ?>
                                        <button type="reset" class="btn">Clear</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title display-4">Staffs</h1>
                            <h6 class="card-subtitle mb-2 text-muted">View all your staffs here.</h6>
                        </div>
                        <table class="table table-hover table-bordered table-responsive" style="background-color: white; margin-bottom: 0;">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" width="5%">ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">Email</th>
                                    <th scope="col" width="1%"></th>
                                    <th scope="col" width="1%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    // Read
                                    try {
                                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                        $stmt = $conn->prepare("SELECT * FROM tbl_staffs_a155652_pt2");
                                        $stmt->execute();
                                        $result = $stmt->fetchAll();
                                    } catch (PDOException $e) {
                                        echo "Error: " . $e->getMessage();
                                    }
                                    foreach ($result as $readrow) {
                                        ?>
                                            <tr>
                                                <th scope="row">
                                                    <?php echo $readrow['fld_staff_num']; ?>
                                                </th>
                                                <td>
                                                    <?php echo $readrow['fld_staff_name']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $readrow['fld_staff_username']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $readrow['fld_staff_email']; ?>
                                                </td>
                                                <td style="text-align: center">
                                                    <a href="staffs.php?edit=<?php echo $readrow['fld_staff_num']; ?>"><span class="oi oi-pencil" title="Edit" aria-hidden="true"></span></a>
                                                </td>
                                                <td style="text-align: center">
                                                    <a href="staffs.php?delete=<?php echo $readrow['fld_staff_num']; ?>" onclick="return confirm('Are you sure to delete?');"><span class="oi oi-delete" title="Delete" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            <?php
                                    }
                                    $conn = null;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once('footer.php'); ?>
        <?php include_once('bootstrap_js.php'); ?>
    </body>

	</html>
