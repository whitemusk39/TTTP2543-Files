<?php
	include_once('session.php');
	verifySession();
?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once('head.php'); ?>
</head>

<body>
  <?php include_once('nav_bar.php'); ?>

  <div class="container">

    <div class="row">
      <div class="col">
        <div class="card">
          <div class="card-body">
            <?php include_once 'products_crud.php';?>
            <h1 class="card-title display-4">Add a new Product</h1>
            <h6 class="card-subtitle mb-2 text-muted">Have a new product? Add them here.</h6>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item">
              <form action="products.php" method="post">
                <div class="form-row">
                  <div class="form-group col-md-3">
                    <label for="ipid">Product ID</label>
                    <input class="form-control" id="ipid" name="pid" type="text" value="<?php if  (isset($_GET['edit'])) { echo $editrow['fld_product_num']; } ?>" placeholder="Product ID" required>
                  </div>
                  <div class="form-group col-md-9">
                    <label for="iname">Name</label>
                    <input class="form-control" id="iname" name="name" type="text" value="<?php if (isset($_GET['edit'])) { echo $editrow['fld_product_name']; } ?>" placeholder="Product Name" required>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-5">
                    <label for="imaterial">Material</label>
                    <select id="imaterial" name="material" class="form-control" required>
                        <?php
                          // Read
                          try {
                            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                            $stmt = $conn->prepare("SELECT * FROM tbl_materials_a155652_pt2");
                            $stmt->execute();
                            $result = $stmt->fetchAll();
                          } catch (PDOException $e) {
                            echo "Error: " . $e->getMessage();
                          }

                          foreach ($result as $readrow) {
                            echo "<option value='".$readrow['fld_material_num']."'";
                            if (isset($_GET['edit'])) {
                              if ($editrow['fld_material']==$readrow['fld_material_num']) {
                              echo "selected";
                              }
                            }
                            echo ">";
                            echo $readrow['fld_material_name'];
                            echo "</option>";
                          }
                        ?>
                      </select>
                  </div>
                  <div class="form-group col-md-5">
                    <label for="iperiod">Period</label>
                    <select name="period" id="iperiod" class="form-control" required>
                        <?php
                        // Read
                        try {
                          $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                          $stmt = $conn->prepare("SELECT * FROM tbl_periods_a155652_pt2");
                          $stmt->execute();
                          $result = $stmt->fetchAll();
                        } catch (PDOException $e) {
                          echo "Error: " . $e->getMessage();
                        }

                        foreach ($result as $readrow) {
                          echo "<option value='".$readrow['fld_period_num']."'";
                          if (isset($_GET['edit'])) {
                            if ($editrow['fld_period']==$readrow['fld_period_num']) {
                              echo "selected";
                            }
                          }
                          echo ">";
                          echo $readrow['fld_period_name'];
                          echo "</option>";
                        }
                        ?>
                      </select>
                  </div>
                  <div class="form-group col-md-2">
                    <label for="iprice">Price</label>
                    <input class="form-control" id="iprice" name="price" type="number" value="<?php if (isset($_GET['edit'])) { echo $editrow['fld_product_price']; } ?>" step="1" min="0" placeholder="0" required>
                  </div>
                  <div class="form-group col-md-3" style="display: none">
                    <label for="iquantity">Quantity</label>
                    <input class="form-control" id="iprice" name="quantity" type="number" min="0" value="<?php if (isset($_GET['edit'])) { echo $editrow['fld_quantity']; } ?>" placeholder="1" step="1">
                  </div>
                </div>
								<div class="form-row">
									<div class="form-group col">
										<label>Description</label>
										<textarea class="form-control" name="description" rows="8" cols="80" maxlength="255"><?php if (isset($_GET['edit'])) { echo $editrow['fld_description']; } ?></textarea>
									</div>
								</div>
                <?php if (isset($_GET['edit'])) { ?>
                <input type="hidden" name="oldpid" value="<?php echo $editrow['fld_product_num']; ?>">
                <button type="submit" name="update" value="Update product"  class="btn btn-primary">Update</button>
                <?php } else { ?>
                <button type="submit" name="create" value="Add product" class="btn btn-primary">Create</button>
                <?php } ?>
                <button type="reset" class="btn">Clear</button>
              </form>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col">
        <div class="card">
          <div class="card-body">
            <h1 class="card-title display-4">Products</h1>
            <h6 class="card-subtitle mb-2 text-muted">View all your products here.</h6>
          </div>
          <table class="table table-hover table-bordered table-responsive" style="background-color: white; margin-bottom: 0;">
            <thead class="thead-light">
              <tr>
                <th scope="col" width="5%">ID</th>
                <th scope="col">Name</th>
                <th scope="col">Material</th>
                <th scope="col">Period</th>
                <th scope="col">Price</th>
                <th scope="col" width="1%"></th>
                <th scope="col" width="1%"></th>
                <th scope="col" width="1%"></th>
              </tr>
            </thead>
            <tbody>
              <?php
                // Read
                try {
                  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                  $stmt = $conn->prepare("SELECT * FROM tbl_products_a155652_pt2, tbl_periods_a155652_pt2, tbl_materials_a155652_pt2 WHERE tbl_products_a155652_pt2.fld_material = tbl_materials_a155652_pt2.fld_material_num AND tbl_products_a155652_pt2.fld_period = tbl_periods_a155652_pt2.fld_period_num");
                  $stmt->execute();
                  $result = $stmt->fetchAll();
                } catch (PDOException $e) {
                  echo "Error: " . $e->getMessage();
                }

                foreach ($result as $readrow) {
              ?>
                <tr>
                  <th scope="row">
                    <?php echo $readrow['fld_product_num']; ?>
                  </th>
                  <td>
                    <?php echo $readrow['fld_product_name']; ?>
                  </td>
                  <td>
                    <?php echo $readrow['fld_material_name']; ?>
                  </td>
                  <td>
                    <?php echo $readrow['fld_period_name']; ?>
                  </td>
                  <td>
                    RM <?php echo $readrow['fld_product_price']; ?>
                  </td>
                  <td style="text-align: center">
                    <a href="product_details.php?pid=<?php echo $readrow['fld_product_num']; ?>"><span class="oi oi-clipboard" title="Details" aria-hidden="true"></span></a>
                  </td>
                  <td style="text-align: center">
                    <a href="products.php?edit=<?php echo $readrow['fld_product_num']; ?>"><span class="oi oi-pencil" title="Edit" aria-hidden="true"></span></a>
                  </td>
                  <td style="text-align: center">
                    <a href="products.php?delete=<?php echo $readrow['fld_product_num']; ?>" onclick="return confirm('Are you sure to delete?');"><span class="oi oi-delete" title="Delete" aria-hidden="true"></span></a>
                  </td>
                </tr>
                <?php } $conn = null; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <?php include_once('footer.php'); ?>
  <?php include_once('bootstrap_js.php'); ?>

</body>

</html>
