<?php
	include_once('session.php');
	verifySession();
?>
<!DOCTYPE html>
    <html>

    <head>
        <?php include_once('head.php'); ?>
    </head>

    <body>
        <?php include_once('nav_bar.php'); ?>

        <div class="container">

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                          <?php
                          include_once 'orders_crud.php';
                          ?>
                            <h1 class="card-title display-4">Add a new Order</h1>
                            <h6 class="card-subtitle mb-2 text-muted">Have a new order? Add them here.</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <form action="orders.php" method="post">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label>Order ID</label>
                                            <input class="form-control" name="oid" type="text" value="<?php if  (isset($_GET['edit'])) { echo $editrow['fld_order_num']; } ?>" placeholder="Order ID" style="text-transform: capitalize;" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Date</label>
                                            <input style="text-transform: capitalize;" class="form-control" name="orderdate" type="text" value="<?php if (isset($_GET['edit'])) { echo $editrow['fld_order_date']; } ?>" placeholder="Order Date" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Staff</label>
                                            <select style="text-transform: capitalize;" id="isid" name="sid" class="form-control" required>
                                              <?php
                                        try {
                                            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                            $stmt = $conn->prepare("SELECT * FROM tbl_staffs_a155652_pt2");
                                            $stmt->execute();
                                            $result = $stmt->fetchAll();
                                        } catch (PDOException $e) {
                                            echo "Error: " . $e->getMessage();
                                        }
                                        foreach ($result as $staffrow) {
                                            ?>
                                                <?php if ((isset($_GET['edit'])) && ($editrow['fld_staff_num']==$staffrow['fld_staff_num'])) {
                                                ?>
                                                <option value="<?php echo $staffrow['fld_staff_num']; ?>" selected>
                                                  <?php echo $staffrow['fld_staff_name']; ?>
                                                </option>
                                                <?php
                                            } else {
                                                ?>
                                                <option value="<?php echo $staffrow['fld_staff_num']; ?>">
                                                  <?php echo $staffrow['fld_staff_name']; ?>
                                                </option>
                                                <?php
                                            } ?>
                                                <?php
                                        } // while
                                        $conn = null;
                                        ?>
                                              </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label>Customer</label>
                                            <select style="text-transform: capitalize;" id="icid" name="cid" class="form-control" required>
                                              <?php
                                        try {
                                            $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                            $stmt = $conn->prepare("SELECT * FROM tbl_customers_a155652_pt2");
                                            $stmt->execute();
                                            $result = $stmt->fetchAll();
                                        } catch (PDOException $e) {
                                            echo "Error: " . $e->getMessage();
                                        }
                                        foreach ($result as $custrow) {
                                            ?>
                                    						<?php if ((isset($_GET['edit'])) && ($editrow['fld_customer_num']==$custrow['fld_customer_num'])) {
                                                ?>
                                    						<option value="<?php echo $custrow['fld_customer_num']; ?>" selected>
                                    							<?php echo $custrow['fld_customer_name']; ?>
                                    						</option>
                                    						<?php
                                            } else {
                                                ?>
                                    						<option value="<?php echo $custrow['fld_customer_num']; ?>">
                                    							<?php echo $custrow['fld_customer_name']; ?>
                                    						</option>
                                    						<?php
                                            } ?>
                                    						<?php
                                        } // while
                                        $conn = null;
                                        ?>
                                              </select>
                                        </div>
                                    </div>
                                    <?php if (isset($_GET['edit'])) { ?>
                                        <input type="hidden" name="oldoid" value="<?php echo $editrow['fld_customer_num']; ?>">
                                        <button type="submit" name="update" class="btn btn-primary">Update</button>
                                    <?php } else { ?>
                                        <button type="submit" name="create" class="btn btn-primary">Create</button>
                                    <?php } ?>
                                        <button type="reset" class="btn">Clear</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title display-4">Orders</h1>
                            <h6 class="card-subtitle mb-2 text-muted">View all your orders here.</h6>
                        </div>
                        <table class="table table-hover table-bordered table-responsive" style="text-transform: capitalize; background-color: white; margin-bottom: 0;">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col" width="5%">ID</th>
                                    <th scope="col" width="17%">Date</th>
                                    <th scope="col">Staff</th>
                                    <th scope="col">Customer</th>
                                    <th scope="col" width="1%"></th>
                                    <th scope="col" width="1%"></th>
                                    <th scope="col" width="1%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    // Read
                                    try {
                                        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                                        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                        $sql = "SELECT * FROM tbl_orders_a155652_pt2, tbl_staffs_a155652_pt2, tbl_customers_a155652_pt2 WHERE ";
                                        $sql = $sql."tbl_orders_a155652_pt2.fld_staff_num = tbl_staffs_a155652_pt2.fld_staff_num and ";
                                        $sql = $sql."tbl_orders_a155652_pt2.fld_customer_num = tbl_customers_a155652_pt2.fld_customer_num";
                                        $stmt = $conn->prepare($sql);
                                        $stmt->execute();
                                        $result = $stmt->fetchAll();
                                    } catch (PDOException $e) {
                                        echo "Error: " . $e->getMessage();
                                    }
                                    foreach ($result as $readrow) {
                                        ?>
                                            <tr>
                                                <th scope="row">
                                                    <?php echo $readrow['fld_order_num']; ?>
                                                </th>
                                                <td>
                                                    <?php echo $readrow['fld_order_date']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $readrow['fld_staff_name']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $readrow['fld_customer_name']; ?>
                                                </td>
                                                <td style="text-align: center">
                                                    <a href="orders_details.php?oid=<?php echo $readrow['fld_order_num']; ?>"><span class="oi oi-clipboard" title="Details" aria-hidden="true"></span></a>
                                                </td>
                                                <td style="text-align: center">
                                                    <a href="orders.php?edit=<?php echo $readrow['fld_order_num']; ?>"><span class="oi oi-pencil" title="Edit" aria-hidden="true"></span></a>
                                                </td>
                                                <td style="text-align: center">
                                                    <a href="orders.php?delete=<?php echo $readrow['fld_order_num']; ?>" onclick="return confirm('Are you sure to delete?');"><span class="oi oi-delete" title="Delete" aria-hidden="true"></span></a>
                                                </td>
                                            </tr>
                                            <?php
                                    }
                                    $conn = null;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once('footer.php'); ?>
        <?php include_once('bootstrap_js.php'); ?>
    </body>

    </html>
