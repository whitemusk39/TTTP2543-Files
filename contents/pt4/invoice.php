<?php
	include_once('session.php');
	verifySession();
?>
<!DOCTYPE html>
    <html>

    <head>
        <?php include_once('head.php'); ?>
    </head>

    <body style="background: white;">

        <div class="container">

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body align-self-center text-center">
                          <?php
                          include_once 'database.php';

													try {
												      $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
												      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
												      $stmt = $conn->prepare("SELECT * FROM tbl_orders_a155652_pt2, tbl_staffs_a155652_pt2,
													  tbl_customers_a155652_pt2, tbl_orders_details_a155652_pt2 WHERE
													  tbl_orders_a155652_pt2.fld_staff_num = tbl_staffs_a155652_pt2.fld_staff_num AND
													  tbl_orders_a155652_pt2.fld_customer_num = tbl_customers_a155652_pt2.fld_customer_num AND
													  tbl_orders_a155652_pt2.fld_order_num = tbl_orders_details_a155652_pt2.fld_order_num AND
													  tbl_orders_a155652_pt2.fld_order_num = :oid");
												      $stmt->bindParam(':oid', $oid, PDO::PARAM_STR);
												      $oid = $_GET['oid'];
												      $stmt->execute();
												      $readrow = $stmt->fetch(PDO::FETCH_ASSOC);
												  } catch (PDOException $e) {
												      echo "Error: " . $e->getMessage();
												  }
												  $conn = null;
												  ?>
                            <h1 class="card-title display-4">Antique Delights Sdn. Bhd.</h1>
                            <h6 class="card-subtitle mb-2 text-muted">Fakulti Teknologi dan Sains Maklumat,</h6>
                            <h6 class="card-subtitle mb-2 text-muted">Universiti Kebangsaan Malaysia,</h6>
                            <h6 class="card-subtitle mb-2 text-muted">43600 Bandar Baru Bangi,</h6>
                            <h6 class="card-subtitle mb-2 text-muted">Selangor.</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <form>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label style="font-weight: bold;">Order ID</label>
                                            <input class="form-control" name="oid" type="text" value="<?php echo $readrow['fld_order_num']; ?>" placeholder="Order ID" style="text-transform: capitalize; background-color: white;" disabled>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label style="font-weight: bold;">Order Date</label>
                                            <input style="text-transform: capitalize; background-color: white;" disabled class="form-control" name="orderdate" type="text" value="<?php echo $readrow['fld_order_date']; ?>" placeholder="Order Date">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label style="font-weight: bold;">Staff</label>
                                            <input class="form-control" name="staff" type="text" value="<?php echo $readrow['fld_staff_name']; ?>" placeholder="Staff Name" style="text-transform: capitalize; background-color: white;" disabled>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label style="font-weight: bold;">Customer</label>
                                            <input class="form-control" name="cust" type="text" value="<?php echo $readrow['fld_customer_name']; ?>" placeholder="Customer Name" style="text-transform: capitalize; background-color: white;" disabled>
                                        </div>
                                    </div>
                                </form>
                            </li>
														<li class="list-group-item text-right">
															<b>Date: </b><?php echo date("d M Y"); ?>
                            </li>
														<li class="list-group-item">
															<table class="table table-hover table-bordered table-responsive" style="text-transform: capitalize; background-color: white; margin-bottom: 0;">
			                            <thead class="thead-light">
			                                <tr>
			                                    <th scope="col" width="1%">No.</th>
			                                    <th scope="col" width="85%">Product</th>
			                                    <th scope="col" width="4%">Quantity</th>
			                                    <th scope="col" width="5%">Price/unit (RM)</th>
			                                    <th scope="col" width="5%">Total (RM)</th>
			                                </tr>
			                            </thead>
			                            <tbody>
			                                <?php
																			$grandtotal = 0;
																			$counter = 1;

			                                    // Read
			                                    try {
																						$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
																						$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
																						$stmt = $conn->prepare("SELECT * FROM tbl_orders_details_a155652_pt2,
																					tbl_products_a155652_pt2 WHERE
																					tbl_orders_details_a155652_pt2.fld_product_num = tbl_products_a155652_pt2.fld_product_num AND
																				fld_order_num = :oid");
																						$stmt->bindParam(':oid', $oid, PDO::PARAM_STR);
																						$oid = $_GET['oid'];
																						$stmt->execute();
																						$result = $stmt->fetchAll();
			                                    } catch (PDOException $e) {
			                                        echo "Error: " . $e->getMessage();
			                                    }
			                                    foreach ($result as $readrow) {
			                                        ?>
			                                            <tr>
			                                                <th scope="row">
			                                                    <?php echo $counter; ?>
			                                                </th>
			                                                <td>
			                                                    <?php echo $readrow['fld_product_name']; ?>
			                                                </td>
			                                                <td>
			                                                    <?php echo $readrow['fld_order_detail_quantity']; ?>
			                                                </td>
			                                                <td>
			                                                    <?php echo $readrow['fld_product_price']; ?>
			                                                </td>
			                                                <td>
			                                                    <?php echo $readrow['fld_product_price']*$readrow['fld_order_detail_quantity']; ?>
			                                                </td>
			                                            </tr>
																									<?php
																								      $grandtotal = $grandtotal + $readrow['fld_product_price']*$readrow['fld_order_detail_quantity'];
																								      $counter++;
																							    	} // while

																							    	$conn = null;
																							   ?>
																								 <tr>
																									 <td colspan="4" align="right"><b>Grand Total</b></td>
																									 <td>
																										 <?php echo $grandtotal ?>
																									 </td>
																								 </tr>
			                            </tbody>
			                        </table>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <?php include_once('bootstrap_js.php'); ?>
    </body>

    </html>
