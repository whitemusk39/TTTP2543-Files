<?php

	include_once('session.php');

	$total_pages = 0;
	$per_page = 6;

	if (isset($_GET["page"])){
		$page = intval($_GET["page"]);
	} else {
		$_GET["page"] = 1;
		$page = $_GET["page"];
	}

	$calc = $per_page * $page;
	$start_from = $calc - $per_page;

	if(isset($_GET['sortby'])) {
		$sortby = $_GET['sortby'];
	} else {
		$_GET['sortby'] = "nameAsc";
		$sortby = $_GET['sortby'];
	}

	if(isset($_GET['query'])) {
		$_GET['query'] = htmlspecialchars($_GET['query']);
		$q = htmlspecialchars($_GET['query']);
		$qq = explode(" ", $q);
	}

?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once('head.php'); ?>
</head>

<body>
  <?php include_once('nav_bar.php'); ?>

  <div class="container">
    <div class="row">
			<!-- Catalog -->
      <div class="col">
        <div class="card">
          <div class="card-body">
            <h1 class="card-title display-4">Catalog</h1>
            <h6 class="card-subtitle mb-2 text-muted">View all our products here.</h6>
          </div>
					<ul class="list-group list-group-flush">
						<li class="list-group-item">
							<form action="catalog.php" method="get">
								<div class="form-row">
									<div class="input-group col-lg-4">
										<span class="input-group-btn">
											<button class="btn btn-success" type="submit">Material</button>
										</span>
										<select class="form-control" name="material">
											<option value="all" selected>Show All</option>
											<?php
												// Read
												try {
													$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
													$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
													$stmt = $conn->prepare("SELECT * FROM tbl_materials_a155652_pt2");
													$stmt->execute();
													$result = $stmt->fetchAll();
												} catch (PDOException $e) {
													echo "Error: " . $e->getMessage();
												}

												foreach ($result as $readrow) {
													echo "<option value='".$readrow['fld_material_num']."'";
													if (isset($_GET['material'])) {
														if ($_GET['material']==$readrow['fld_material_num']) {
														echo "selected";
														}
													}
													echo ">";
													echo $readrow['fld_material_name'];
													echo "</option>";
												}
											?>
										</select>
									</div>
									<div class="input-group col-lg-3">
										<span class="input-group-btn">
											<button class="btn btn-warning" type="submit">Period</button>
										</span>
										<select name="period" id="iperiod" class="form-control" required>
											<option value="all" selected>Show All</option>
												<?php
												// Read
												try {
													$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
													$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
													$stmt = $conn->prepare("SELECT * FROM tbl_periods_a155652_pt2");
													$stmt->execute();
													$result = $stmt->fetchAll();
												} catch (PDOException $e) {
													echo "Error: " . $e->getMessage();
												}

												foreach ($result as $readrow) {
													echo "<option value='".$readrow['fld_period_num']."'";
													if (isset($_GET['period'])) {
														if ($_GET['period']==$readrow['fld_period_num']) {
															echo "selected";
														}
													}
													echo ">";
													echo $readrow['fld_period_name'];
													echo "</option>";
												}
												?>
											</select>
									</div>
									<div class="input-group col-lg-3">
										<span class="input-group-btn">
											<button class="btn btn-secondary" type="submit">Sort by</button>
										</span>
										<select class="form-control" name="sortby">
											<option <?php if($_GET['sortby'] == "nameAsc") echo "selected"; ?> value="nameAsc">Name (A-Z)</option>
											<option <?php if($_GET['sortby'] == "nameDesc") echo "selected"; ?> value="nameDesc">Name (Z-A)</option>
											<option <?php if($_GET['sortby'] == "priceAsc") echo "selected"; ?> value="priceAsc">Price (ascending)</option>
											<option <?php if($_GET['sortby'] == "priceDesc") echo "selected"; ?> value="priceDesc">Price (descending)</option>
										</select>
									</div>
									<div class="input-group col-lg-1">
										<span class="input-group-btn">
											<button class="btn btn-primary" type="submit" style="padding-left: 1.3em; padding-right: 1.3em;">Filter</button>
										</span>
									</div>
									<div class="input-group col-lg-1">
										<span class="input-group-btn">
											<a href="catalog.php"><button class="btn btn-danger" type="button" style="padding-left: 1.3em; padding-right: 1.3em;">Reset</button></a>
										</span>
									</div>
								</div>
							</form>
						</li>
						<li class="list-group-item">
							<form action="catalog.php" method="get">
								<div class="form-row">
									<div class="input-group col">
										<span class="input-group-btn">
											<button class="btn btn-secondary" type="button"><a href="adv_search.php" style="color: white">Advanced Mode</a></button></a>
										</span>
										<input type="text" name="query" placeholder="Search for..." class="form-control" value="<?php if(isset($_GET['query'])) echo $_GET['query']; ?>">
										<span class="input-group-btn">
											<button class="btn btn-primary" type="submit">Search</button>
										</span>
									</div>
								</div>
							</form>
						</li>
						<li class="list-group-item">
							<?php
									$filter = "";
									if(isset($_GET['material']) && $_GET['material'] != "all") {
										$material = $_GET['material'];
										$filter .= " AND tbl_products_a155652_pt2.fld_material LIKE '$material' ";
									}

									if(isset($_GET['period']) && $_GET['period'] != "all") {
										$period = $_GET['period'];
										$filter .= " AND tbl_products_a155652_pt2.fld_period LIKE '$period' ";
									}

									$sort = "";
									switch($sortby) {
										case "priceAsc":
											$sort .= " fld_product_price ASC ";
											break;
										case "priceDesc":
											$sort .= " fld_product_price DESC ";
											break;
										case "nameAsc":
											$sort .= " fld_product_name ASC ";
											break;
										case "nameDesc":
											$sort .= " fld_product_name DESC ";
											break;
									}

									$search = "";
									if(isset($_GET['query'])) {
										foreach($qq as $w) {
											// $search .= " OR (fld_product_name LIKE '%$w%') OR (fld_description LIKE '%$w%') OR (fld_material_name LIKE '%$w%') OR (fld_period_name LIKE '%$w%') ";
											$search .= " AND (fld_product_name LIKE '%$w%') OR (fld_description LIKE '%$w%') AND (fld_material LIKE '%$w%') OR (fld_period LIKE '%$w%') ";
										}
									}

	                try {
	                  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
	                  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	                  $stmt = $conn->prepare("SELECT * FROM tbl_products_a155652_pt2, tbl_periods_a155652_pt2, tbl_materials_a155652_pt2 WHERE tbl_products_a155652_pt2.fld_material = tbl_materials_a155652_pt2.fld_material_num AND tbl_products_a155652_pt2.fld_period = tbl_periods_a155652_pt2.fld_period_num $filter $search ORDER BY $sort LIMIT $start_from, $per_page ");
	                  $stmt->execute();
	                  $result = $stmt->fetchAll();
	                } catch (PDOException $e) {
	                  echo "Error: " . $e->getMessage();
	                }

									if(isset($_GET['query'])) {
										if(count($result ) == 0) {
											echo '<div class="alert alert-danger" role="alert" style="margin-top: 1em;">No products matched your description.</div>';
										}
									}


								$i = 2;
								foreach ($result as $readrow) {
									if($i == 2) {
										echo "<div class='row' style='margin-bottom: 2em;'>";
									}?>
									<div class="col-lg-6">
										<div class="card h-100">
											<ul class="list-group list-group-flush">
												<li class="list-group-item">
													<ul class="list-inline">
														<li class="list-inline-item"><a href="catalog.php?filter=true&period=<?php echo $readrow['fld_period'] ?>" class="badge badge-warning"><?php echo $readrow['fld_period_name'] ?></a></li>
														<li class="list-inline-item"><a href="catalog.php?filter=true&material=<?php echo $readrow['fld_material'] ?>" class="badge badge-success"><?php echo $readrow['fld_material_name'] ?></a></li>
														<li class="list-inline-item float-right" style="color: #444444;"><b>RM <?php echo $readrow['fld_product_price'] ?></b></li>
													</ul>
												</li>
											</ul>
											<img style="height: 320px; width: auto; object-fit: cover;" class="card-img-top" src="assets/products/<?php echo $readrow['fld_product_num'] ?>.jpg" alt="<?php echo $readrow['fld_product_name'] ?>">
											<div class="card-body">
												<h4 class="card-title"><?php echo $readrow['fld_product_name'] ?></h4>
												<p class="card-text" style="color: #7a7a7a;"><em><?php echo $readrow['fld_description'] ?></em></p>
											</div>
											<ul class="list-group list-group-flush">
												<li class="list-group-item">
													<a href="product_details.php?pid=<?php echo $readrow['fld_product_num']; ?>" class="btn btn-primary btn-block">View Product</a>
												</li>
											</ul>

										</div>
									</div>
								<?php if($i == 1) { echo "</div>"; } $i--; if($i == 0) {$i = 2;} } ?>
						</li>
						<li class="list-group-item">
							<?php
								$total_pages = 0;
								try {
                  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
                  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

									$sort = "";
									switch($sortby) {
										case "priceAsc":
											$sort = "fld_product_price ASC ";
											break;
										case "priceDesc":
											$sort = "fld_product_price DESC ";
											break;
										case "nameAsc":
											$sort = "fld_product_name ASC ";
											break;
										case "nameDesc":
											$sort = "fld_product_name DESC ";
											break;
									}

									$search = "";
									if(isset($_GET['query'])) {
										foreach($qq as $w) {
											$search .= " AND (fld_product_name LIKE '%$w%') OR (fld_description LIKE '%$w%') AND (fld_material LIKE '%$w%') OR (fld_period LIKE '%$w%') ";
										}
									}

									$filter = "";
									if(isset($_GET['material']) && isset($_GET['period']) && $_GET['material'] != "all" && $_GET['period'] != "all") {
										$material = $_GET['material'];
										$period = $_GET['period'];
										$filter .= " AND tbl_products_a155652_pt2.fld_material LIKE '$material' AND tbl_products_a155652_pt2.fld_period LIKE '$period' ";
									} else if(isset($_GET['material']) && $_GET['material'] != "all") {
										$material = $_GET['material'];
										$filter .= " AND tbl_products_a155652_pt2.fld_material LIKE '$material' ";
									} else if(isset($_GET['period']) && $_GET['period'] != "all") {
										$period = $_GET['period'];
										$filter .= " AND tbl_products_a155652_pt2.fld_period LIKE '$period' ";
									}

									$stmt = $conn->prepare("SELECT * FROM tbl_products_a155652_pt2, tbl_periods_a155652_pt2, tbl_materials_a155652_pt2 WHERE tbl_products_a155652_pt2.fld_material = tbl_materials_a155652_pt2.fld_material_num AND tbl_products_a155652_pt2.fld_period = tbl_periods_a155652_pt2.fld_period_num $search $filter ORDER BY $sort");

                  $stmt->execute();
                  $result = $stmt->fetchAll();
									$total_records = count($result);
									$total_pages = ceil($total_records / $per_page);
                } catch (PDOException $e) {
                  echo "Error: " . $e->getMessage();
                }
							?>
							<nav aria-label="Page navigation example">
							  <ul class="pagination justify-content-end">
									<?php if ($page == 1) { ?> <li class="page-item disabled"> <?php } else { ?> <li class="page-item"> <?php } ?>
										<?php
											$urlFilter = "";
											if(isset($_GET['material'])) {
												$urlFilter .= "&material=".$_GET['material'];
											}

											if(isset($_GET['period'])) {
												$urlFilter .= "&period=".$_GET['period'];
											}

											$urlQuery = "";
											if(isset($_GET['query'])) {
												$urlQuery .= "&query=".htmlspecialchars($_GET['query']);
											}

											switch($sortby) {
												case "priceAsc":
													$urlSort = "&sortby=priceAsc";
													break;
												case "priceDesc":
													$urlSort = "&sortby=priceDesc";
													break;
												case "nameAsc":
													$urlSort = "&sortby=nameAsc";
													break;
												case "nameDesc":
													$urlSort = "&sortby=nameDesc";
													break;
											}

											$urlAll = $urlFilter.$urlQuery.$urlSort;
										?>
							      <a class="page-link" href="catalog.php?page=<?php echo $page-1; echo $urlAll; ?>" tabindex="-1">Previous</a>
							    </li>
									<?php
										$links = "";
										$current_page = $_GET['page'];

								    if($total_pages == 1) {
											$links .= '<li class="page-item active"><a class="page-link" href="catalog.php?page=1'.$urlAll.'">1</a></li>';
										} else if ($total_pages >= 1 && $current_page <= $total_pages) {
											if($current_page == 1) {
												$links .= '<li class="page-item active"><a class="page-link" href="catalog.php?page=1'.$urlAll.'">1</a></li>';
											} else {
												$links .= '<li class="page-item"><a class="page-link" href="catalog.php?page=1'.$urlAll.'">1</a></li>';
											}

								        $i = max(2, $current_page - 1);
								        if ($i > 2)
								            $links .= '<li class="page-item disabled"><a class="page-link">...</a></li>';
								        for (; $i < min($current_page + 2, $total_pages); $i++) {
													if($i == $current_page) {
														$links .= '<li class="page-item active"><a class="page-link" href="catalog.php?page='.$i.''.$urlAll.'">'.$i.'</a></li>';
													} else {
														$links .= '<li class="page-item "><a class="page-link" href="catalog.php?page='.$i.''.$urlAll.'">'.$i.'</a></li>';
													}
								        }
								        if ($i != $total_pages)
								            $links .= '<li class="page-item disabled"><a class="page-link">...</a></li>';
														if($current_page == $total_pages) {
															$links .= '<li class="page-item active"><a class="page-link" href="catalog.php?page='.$total_pages.''.$urlAll.'">'.$total_pages.'</a></li>';
														} else {
															$links .= '<li class="page-item"><a class="page-link" href="catalog.php?page='.$total_pages.''.$urlAll.'">'.$total_pages.'</a></li>';
														}

								    }

										echo $links;

									?>
									<?php if ($page == $total_pages || $total_pages == 0) { ?> <li class="page-item disabled"> <?php } else { ?> <li class="page-item"> <?php } ?>
							      <a class="page-link" href="catalog.php?page=<?php echo $page+1; echo $urlAll;  ?>" tabindex="-1">Next</a>
							    </li>
							  </ul>
							</nav>
						</li>
					</ul>
        </div>
      </div>
    </div>
  </div>

  <?php include_once('footer.php'); ?>
  <?php include_once('bootstrap_js.php'); ?>

</body>

</html>
