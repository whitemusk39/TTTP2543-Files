<?php

function titleCase($s)
{
    $s = ucwords(strtolower($s));
    $start = intval(strpos($s, stristr($s, "(")));

    if ($start <> 0) {
        $end = intval(strpos($s, stristr($s, ")")));
        $result = substr($s, ($start+1), (($end - $start)-1));
        $s = str_replace($result, strtoupper($result), $s);
    }

    return $s;
}

include_once 'database.php';

$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if (isset($_POST['create']) || isset($_POST['update'])) {

    /**
    *   Initialise variables.
    *
    *   Use array because it will be much easier to
    *   manage and used together with foreach.
    */
    $vars = array(
        "cid" => "",
        "name" => ""
    );

    $count_error = 0;
    $msg = "";

    /**
    *    Validate submitted data.
    *
    *   If empty show error message,
    *   else put in local variables.
    */

    foreach ($_POST as $key=>$value) {

        /**
        *   Set the fieldName.
        *
        *   To make the error messages prettier. It will
        *   use the field's title instead of the field's ID.
        */

        $fieldName = "";
        switch ($key) {
            case "cid":
                $fieldName = "Customer ID";
                break;
            case "name":
                $fieldName = "Customer Name";
                break;
            default:
                $fieldName = $key;
                break;
        }

        // Check if the value is set and not empty
        if ($key == "cid"){
          try {
              $stmt = $conn->prepare("SELECT fld_customer_num FROM tbl_customers_a155652_pt2 WHERE fld_customer_num = :findcid");
              $stmt->bindParam(':findcid', $findcid, PDO::PARAM_STR);
              $findcid = $value;
              $stmt->execute();
              $result = $stmt->fetch(PDO::FETCH_ASSOC);
              if(empty($result["fld_customer_num"]) && !isset($result["fld_customer_num"])) {
                $vars[$key] = $value;
              } else if(isset($_POST['update'])) {
                if($_POST['oldcid'] == $result["fld_customer_num"]) {
                  $vars[$key] = $value;
                }
              } else {
                $msg .= "Error: Customer ID already exist!<br />";
                $count_error++;
              }
          } catch (PDOException $e) {
              $msg .= "Error: " . $e->getMessage() . "<br />";
          }
        } else if (isset($value) && !empty($value)) {
            $vars[$key] = $value;
        } else {
            $msg .= "Error: Field '". $fieldName ."' is not filled in.<br />";
            $count_error++;
        }
    }

    // Display error messages if there's any error.
    if ($count_error > 0) {
        echo $msg;
        echo "$count_error error(s) detected.<br />";
        echo "<button class='btn' onclick='history.go(-1);'>Back </button>";
        die();
    }
  }

//Create
if (isset($_POST['create'])) {

  try {

    $stmt = $conn->prepare("INSERT INTO tbl_customers_a155652_pt2(fld_customer_num, fld_customer_name) VALUES(:cid, :name)");

    $stmt->bindParam(':cid', $cid, PDO::PARAM_STR);
    $stmt->bindParam(':name', $name, PDO::PARAM_STR);

    $cid = $_POST['cid'];
    $name = titleCase($_POST['name']);


    $stmt->execute();
    }

  catch(PDOException $e)
  {
      echo "Error: " . $e->getMessage();
  }
}

//Update
if (isset($_POST['update'])) {

  try {

    $stmt = $conn->prepare("UPDATE tbl_customers_a155652_pt2 SET
    fld_customer_num = :cid,
    fld_customer_name = :name
    WHERE fld_customer_num = :oldcid");

    $stmt->bindParam(':cid', $cid, PDO::PARAM_STR);
    $stmt->bindParam(':name', $name, PDO::PARAM_STR);
    $stmt->bindParam(':oldcid', $oldcid, PDO::PARAM_STR);

    $cid = $_POST['cid'];
    $name = $_POST['name'];
    $oldcid = $_POST['oldcid'];

    $stmt->execute();

    if(!isset($_SESSION['login_user'])) {
      header("Location: customers.php");
    }
    }

  catch(PDOException $e)
  {
      echo "Error: " . $e->getMessage();
  }
}

//Delete
if (isset($_GET['delete'])) {

  try {

    $stmt = $conn->prepare("DELETE FROM tbl_customers_a155652_pt2 WHERE fld_customer_num = :cid");

    $stmt->bindParam(':cid', $cid, PDO::PARAM_STR);

    $cid = $_GET['delete'];

    $stmt->execute();

    if(!isset($_SESSION['login_user'])) {
      header("Location: customers.php");
    }
    }

  catch(PDOException $e)
  {
      echo "Error: " . $e->getMessage();
  }
}

//Edit
if (isset($_GET['edit'])) {

  try {

    $stmt = $conn->prepare("SELECT * FROM tbl_customers_a155652_pt2 WHERE fld_customer_num = :cid");

    $stmt->bindParam(':cid', $cid, PDO::PARAM_STR);

    $cid = $_GET['edit'];

    $stmt->execute();

    $editrow = $stmt->fetch(PDO::FETCH_ASSOC);
    }

  catch(PDOException $e)
  {
      echo "Error: " . $e->getMessage();
  }
}

  $conn = null;

?>
