<?php include_once 'login_process.php'; ?>
<!DOCTYPE html>
	<html>

	<head>
        <?php include_once('head.php'); ?>
    </head>

    <body>
        <?php include_once('nav_bar.php'); ?>

        <div class="container">
					<?php echo $err; ?>
            <div class="row justify-content-md-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="card-title display-4">Login</h1>
                            <h6 class="card-subtitle mb-2 text-muted">Please log into your account.</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <form action="login.php" method="post">
                                    <div class="form-row">
                                        <div class="form-group col">
                                            <label>Username</label>
                                            <input class="form-control" name="lusername" type="text" value="<?php if (isset($_POST['login'])) { echo $_POST['lusername']; } ?>" placeholder="Username">
                                        </div>
                                    </div>
																		<div class="form-row">
																			<div class="form-group col">
																					<label>Password</label>
																					<input class="form-control" name="lpassword" type="password" placeholder="Password">
																			</div>
                                    </div>
                                    <button type="submit" value="login" name="login" class="btn btn-primary">Login</button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <?php include_once('footer.php'); ?>
        <?php include_once('bootstrap_js.php'); ?>
    </body>

	</html>
