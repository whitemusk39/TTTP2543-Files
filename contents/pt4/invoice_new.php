<?php
  include_once('session.php');
  verifySession();
  include_once 'database.php';
?>
<?php
try {
  $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT * FROM tbl_orders_a155652_pt2, tbl_staffs_a155652_pt2,
        tbl_customers_a155652_pt2, tbl_orders_details_a155652_pt2 WHERE
        tbl_orders_a155652_pt2.fld_staff_num = tbl_staffs_a155652_pt2.fld_staff_num AND
        tbl_orders_a155652_pt2.fld_customer_num = tbl_customers_a155652_pt2.fld_customer_num AND
        tbl_orders_a155652_pt2.fld_order_num = tbl_orders_details_a155652_pt2.fld_order_num AND
        tbl_orders_a155652_pt2.fld_order_num = :oid");
  $stmt->bindParam(':oid', $oid, PDO::PARAM_STR);
    $oid = $_GET['oid'];
  $stmt->execute();
  $readrow = $stmt->fetch(PDO::FETCH_ASSOC);
  }
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

  <title>Invoice</title>

  <!-- Bootstrap -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">


</head>
<body style="font-family: 'Quicksand', sans-serif;">
  <div class="container">
    <div class="row">
    <div class="col-xs-6" >
      <h1 style="font-size: 4em;">Antique Delights</h1>
      <h4 style="font-style: italic;">"Live in the Past. Fashionably."</h4>
    </div>
    <div class="col-xs-6 text-right">
      <h1>INVOICE</h1>
      <h5>Order ID: <?php echo $readrow['fld_order_num'] ?></h5>
      <h5>Date: <?php echo $readrow['fld_order_date'] ?></h5>
    </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-xs-5">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4>From: Antique Delights Sdn. Bhd.</h4>
          </div>
          <div class="panel-body">
            <p>
            No. 75, Jalan Bangi Baru 7 <br>
            Taman Bangi 7, Bangi  <br>
            899007 <br>
            Bangiland, Bangisia <br>
            </p>
          </div>
        </div>
      </div>
        <div class="col-xs-5 col-xs-offset-2 text-right">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <h4>To: <?php echo $readrow['fld_customer_name']?></h4>
                </div>
                <div class="panel-body">
            <p>
            Address 1 <br>
            Address 2 <br>
            Postcode City <br>
            State <br>
            </p>
                </div>
            </div>
        </div>
    </div>

    <table class="table table-bordered">
      <tr>
        <th>No</th>
        <th>Product</th>
        <th class="text-right">Quantity</th>
        <th class="text-right">Price (RM)/unit</th>
        <th class="text-right">Total (RM)</th>
      </tr>
      <?php
      $grandtotal = 0;
      $counter = 1;
      try {
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $stmt = $conn->prepare("SELECT * FROM tbl_orders_details_a155652_pt2,
                tbl_products_a155652_pt2 where
                tbl_orders_details_a155652_pt2.fld_product_num = tbl_products_a155652_pt2.fld_product_num AND
                fld_order_num = :oid");
        $stmt->bindParam(':oid', $oid, PDO::PARAM_STR);
          $oid = $_GET['oid'];
        $stmt->execute();
        $result = $stmt->fetchAll();
      }
      catch(PDOException $e){
            echo "Error: " . $e->getMessage();
      }
      foreach($result as $detailrow) {
      ?>
      <tr>
        <td><?php echo $counter; ?></td>
        <td><?php echo $detailrow['fld_product_name']; ?></td>
        <td class="text-right"><?php echo $detailrow['fld_order_detail_quantity']; ?></td>
        <td class="text-right"><?php echo $detailrow['fld_product_price']; ?></td>
        <td class="text-right"><?php echo $detailrow['fld_product_price']*$detailrow['fld_order_detail_quantity']; ?></td>
      </tr>
      <?php
        $grandtotal = $grandtotal + $detailrow['fld_product_price']*$detailrow['fld_order_detail_quantity'];
        $counter++;
      } // while
      ?>
      <tr>
        <td colspan="4" class="text-right"><b>Grand Total</b></td>
        <td class="text-right"><?php echo $grandtotal ?></td>
      </tr>
    </table>

    <div class="row">
      <div class="col-xs-5">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4>Bank Details</h4>
          </div>
          <div class="panel-body">
            <p>Your Name<br />
            Bank Name</p>
            <p>SWIFT:<br />
            Account Number:<br />
            IBAN:</p>
          </div>
        </div>
        </div>
      <div class="col-xs-7">
        <div class="span7">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4>Contact Details</h4>
            </div>
            <div class="panel-body">
              <p>Staff: <?php echo $readrow['fld_staff_name'] ?><br>
              Email: <?php echo $readrow['fld_staff_email'] ?></p>
              <p><br /><br />Computer-generated invoice. No signature is required.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
