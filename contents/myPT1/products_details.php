<!DOCTYPE html>
<html>

<head>
	<title>My Antiques Ordering System : Products Details</title>
</head>

<body>
	<center>
		<a href="index.php">Home</a> |
		<a href="products.php">Products</a> |
		<a href="customers.php">Customers</a> |
		<a href="staffs.php">Staffs</a> |
		<a href="orders.php">Orders</a>
		<div style="min-height: 100vh" id="P001">
				<hr> Product ID: P001
				<br> Name: Outstanding Set of 12 Victorian Walnut Dining Chairs
				<br> Price: RM 8501.00
				<br> Material: Walnut
				<br> Manufacturing Period: 19th Century
				<br>
				<img src="products/001.jpg" width="30%" height="30%">
			</a>
		</div>
		<div style="min-height: 100vh" id="P002">
				<hr> Product ID: P002
				<br> Name: Late 17th Century English Oak Joint Stool
				<br> Price: RM 966.00
				<br> Material: Oak
				<br> Manufacturing Period: 17th Century
				<br>
				<img src="products/002.jpg" width="30%" height="30%">
			</a>
		</div>
		<div style="min-height: 100vh" id="P003">
				<hr> Product ID: P003
				<br> Name: Rare Pair of 19th Century Primitive Welsh Stick Chairs
				<br> Price: RM 1345.00
				<br> Material: Ash
				<br> Manufacturing Period: 19th Century
				<br>
				<img src="products/003.jpg" width="30%" height="30%">
			</a>
		</div>
		<div style="min-height: 100vh" id="P004">
				<hr> Product ID: P004
				<br> Name: 19th Century Country Poultry Stool
				<br> Price: RM 300.00
				<br> Material: Elm
				<br> Manufacturing Period: 19th Century
				<br>
				<img src="products/004.jpg" width="30%" height="30%">
			</a>
		</div>
		<div style="min-height: 100vh" id="P005">
				<hr> Product ID: P005
				<br> Name: 18th Century Georgian Oak Corner Cupboard
				<br> Price: RM 721.00
				<br> Material: Oak
				<br> Manufacturing Period: 18th Century
				<br>
				<img src="products/005.jpg" width="30%" height="30%">
			</a>
		</div>
		<div style="min-height: 100vh" id="P006">
				<hr> Product ID: P006
				<br> Name: 18th Century Oak Mule Chest c.1750
				<br> Price: RM 915.00
				<br> Material: Oak
				<br> Manufacturing Period: 18th Century
				<br>
				<img src="products/006.jpg" width="30%" height="30%">
			</a>
		</div>
		<div style="min-height: 100vh" id="P007">
				<hr> Product ID: P007
				<br> Name: Superb Original William IV Mirror Back Rosewood Gilt Console Table / Hall Table
				<br> Price: RM 915.00
				<br> Material: Rosewood
				<br> Manufacturing Period: 19th Century
				<br>
				<img src="products/007.jpg" width="30%" height="30%">
			</a>
		</div>
		<div style="min-height: 100vh" id="P008">
				<hr> Product ID: P008
				<br> Name: George III Mahogany Lowboy c.1760
				<br> Price: RM 793.00
				<br> Material: Mahogany
				<br> Manufacturing Period: 18th Century
				<br>
				<img src="products/008.jpg" width="30%" height="30%">
			</a>
		</div>
		<div style="min-height: 100vh" id="P009">
				<hr> Product ID: P009
				<br> Name: Jacobean Style Carved Oak 2 Drawer Side Table c.1920
				<br> Price: RM 1529.00
				<br> Material: Oak
				<br> Manufacturing Period: 20th Century
				<br>
				<img src="products/009.jpg" width="30%" height="30%">
			</a>
		</div>
		<div style="min-height: 100vh" id="P010">
				<hr> Product ID: P010
				<br> Name: Fabulous Quality Mahogany French Double Bed c.1870
				<br> Price: RM 3975.00
				<br> Material: Mahogany
				<br> Manufacturing Period: 19th Century
				<br>
				<img src="products/010.jpg" width="30%" height="30%">
			</a>
		</div>
	</center>
</body>

</html>
