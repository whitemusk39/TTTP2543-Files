<!DOCTYPE html>
<html>

<head>
	<title>My Antiques Ordering System : Products</title>
</head>

<body>
	<center>
		<a href="index.php">Home</a> |
		<a href="products.php">Products</a> |
		<a href="customers.php">Customers</a> |
		<a href="staffs.php">Staffs</a> |
		<a href="orders.php">Orders</a>
		<hr>
		<form action="products.php" method="post">
			Product ID
			<input name="pid" type="text">
			<br> Name
			<input name="name" type="text">
			<br> Price
			<input name="price" type="text">
			<br> Material
			<select name="material">
				<option value="Mahogany">Mahogany</option>
				<option value="Oak">Oak</option>
				<option value="Walnut">Walnut</option>
				<option value="Ash">Ash</option>
				<option value="Mahogany">Mahogany</option>
				<option value="Elm">Elm</option>
				<option value="Rosewood">Rosewood</option>
			</select>
			<br> Manufacturing Period
			<select name="year">
				<option value="17th">17th Century</option>
				<option value="18th">18th Century</option>
				<option value="19th">19th Century</option>
				<option value="20th">20th Century</option>
			</select>
			<br>
			<button type="submit" name="create">Create</button>
			<button type="reset">Clear</button>
		</form>
		<hr>
		<table border="1">
			<tr>
				<td>Product ID</td>
				<td>Name</td>
				<td>Price</td>
				<td>Material</td>
				<td>Manufacturing Period</td>
				<td></td>
			</tr>
			<tr>
				<td>P001</td>
				<td>Small Rare Oak 18th Century Coffer</td>
				<td>966</td>
				<td>Oak</td>
				<td>18th Century</td>
				<td>
					<a href="products_details.php#P001">Details</a>
					<a href="products.php">Edit</a>
					<a href="products.php">Delete</a>
				</td>
			</tr>
			<tr>
				<td>P002</td>
				<td>Late 17th Century English Oak Joint Stool</td>
				<td>966</td>
				<td>Oak</td>
				<td>17th Century</td>
				<td>
					<a href="products_details.php#P002">Details</a>
					<a href="products.php">Edit</a>
					<a href="products.php">Delete</a>
				</td>
			</tr>
			<tr>
				<td>P003</td>
				<td>Rare Pair of 19th Century Primitive Welsh Stick Chairs</td>
				<td>1345</td>
				<td>Ash</td>
				<td>19th Century</td>
				<td>
					<a href="products_details.php#P003">Details</a>
					<a href="products.php">Edit</a>
					<a href="products.php">Delete</a>
				</td>
			</tr>
			<tr>
				<td>P004</td>
				<td>19th Century Country Poultry Stool</td>
				<td>300</td>
				<td>Elm</td>
				<td>19th Century</td>
				<td>
					<a href="products_details.php#P004">Details</a>
					<a href="products.php">Edit</a>
					<a href="products.php">Delete</a>
				</td>
			</tr>
			<tr>
				<td>P005</td>
				<td>18th Century Georgian Oak Corner Cupboard</td>
				<td>721</td>
				<td>Oak</td>
				<td>18th Century</td>
				<td>
					<a href="products_details.php#P005">Details</a>
					<a href="products.php">Edit</a>
					<a href="products.php">Delete</a>
				</td>
			</tr>
			<tr>
				<td>P006</td>
				<td>18th Century Oak Mule Chest c.1750</td>
				<td>915</td>
				<td>Oak</td>
				<td>18th Century</td>
				<td>
					<a href="products_details.php#P006">Details</a>
					<a href="products.php">Edit</a>
					<a href="products.php">Delete</a>
				</td>
			</tr>
			<tr>
				<td>P007</td>
				<td>Superb Original William IV Mirror Back Rosewood Gilt Console Table / Hall Table</td>
				<td>915</td>
				<td>Rosewood</td>
				<td>19th Century</td>
				<td>
					<a href="products_details.php#P007">Details</a>
					<a href="products.php">Edit</a>
					<a href="products.php">Delete</a>
				</td>
			</tr>
			<tr>
				<td>P008</td>
				<td>George III Mahogany Lowboy c.1760</td>
				<td>793</td>
				<td>Mahogany</td>
				<td>18th Century</td>
				<td>
					<a href="products_details.php#P008">Details</a>
					<a href="products.php">Edit</a>
					<a href="products.php">Delete</a>
				</td>
			</tr>
			<tr>
				<td>P009</td>
				<td>Jacobean Style Carved Oak 2 Drawer Side Table c.1920</td>
				<td>1529</td>
				<td>Oak</td>
				<td>20th Century</td>
				<td>
					<a href="products_details.php#P009">Details</a>
					<a href="products.php">Edit</a>
					<a href="products.php">Delete</a>
				</td>
			</tr>
			<tr>
				<td>P010</td>
				<td>Fabulous Quality Mahogany French Double Bed c.1870</td>
				<td>3975</td>
				<td>Mahogany</td>
				<td>19th Century</td>
				<td>
					<a href="products_details.php#P010">Details</a>
					<a href="products.php">Edit</a>
					<a href="products.php">Delete</a>
				</td>
			</tr>
		</table>
	</center>
</body>

</html>
